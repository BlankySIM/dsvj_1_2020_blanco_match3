#include "game.h"

namespace match3 {

	static void resiseGame() {
		if (screens::resizeWindow) {
			sprites::init();
			button::initButtonSizes();
			tile::init();
			player::resize();
			enemy::init();
			credits::resize();
			gameplay::resize();
			intructions::resize();
			menu::resize();
			results::resize();
			screens::resizeWindow = false;
		}
	}
	static void init() {
		InitWindow(screens::screenWidth, screens::screenHeight, "PuzzleMatch");
		SetTargetFPS(screens::targetFPS);
		InitAudioDevice();
		menu::init();
		gameplay::init();
		credits::init();
		intructions::init();
		results::init();
	}
	static void inputs() {
		switch (screens::currentScreen) {
		case screens::gameplay_screen:
			gameplay::inputs();
			break;
		case screens::menu_screen:
			menu::inputs();
			break;
		case screens::credits_screen:
			credits::inputs();
			break;
		case screens::instructions_screen:
			intructions::inputs();
			break;
		case screens::results_screen:
			results::inputs();
			break;
		}
	}
	static void update() {
		switch (screens::currentScreen) {
		case screens::gameplay_screen:
			gameplay::update();
			break;
		case screens::menu_screen:
			menu::update();
			break;
		case screens::credits_screen:
			credits::update();
			break;
		case screens::instructions_screen:
			intructions::update();
			break;
		case screens::results_screen:
			results::update();
			break;
		}
	}
	static void draw() {
		resiseGame();
		BeginDrawing();
		switch (screens::currentScreen) {
		case screens::gameplay_screen:
			gameplay::draw();
			break;
		case screens::menu_screen:
			menu::draw();
			break;
		case screens::credits_screen:
			credits::draw();
			break;
		case screens::instructions_screen:
			intructions::draw();
			break;
		case screens::results_screen:
			results::draw();
			break;
		}
		ClearBackground(BLACK);
		EndDrawing();
	}
	static void deinit() {
		gameplay::deinit();
		menu::deinit();
		credits::deinit();
		intructions::deinit();
		results::deinit();
		CloseWindow();
	}
	void run(){
		match3::init();
		while (!WindowShouldClose()){
			match3::inputs();
			match3::update();
			match3::draw();
		}
		match3::deinit();
	}
}