#ifndef BUTTON_H
#define	BUTTON_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "sprites/sprites.h"
#include "audio/audio.h"

namespace match3 {
	namespace button {

		extern float normalButtonW;
		extern float normalButtonH;
		extern float tinyButtonW;
		extern float tinyButtonH;
		struct BUTTON {
			Rectangle rec;
			bool active;
		};

		void initButtonSizes();
		void update(BUTTON &buttonToCheck, Vector2 mousePosition);
		void updateMuteButton(BUTTON &buttonToCheck, Vector2 mousePosition);
		void draw(BUTTON buttonToDraw, Vector2 mousePosition);
		void drawNextButton(BUTTON buttonToDraw, Vector2 mousePosition);
		void drawPreviousButton(BUTTON buttonToDraw, Vector2 mousePosition);
		void drawPauseButton(BUTTON buttonToDraw, Vector2 mousePosition);
		void drawMuteButton(BUTTON buttonToDraw, Vector2 mousePosition);
		void drawSettingsButton(BUTTON buttonToDraw, Vector2 mousePosition);
	}
}
#endif
