#include "button.h"

namespace match3 {
	namespace button {

		float normalButtonW = 0;
		float normalButtonH = 0;
		float tinyButtonW = 0;
		float tinyButtonH = 0;

		void initButtonSizes() {
			normalButtonW = static_cast<float>(sprites::normalButton1.width);
			normalButtonH = static_cast<float>(sprites::normalButton1.height);
			tinyButtonW =   static_cast<float>(sprites::nextButton1.width);
			tinyButtonH =   static_cast<float>(sprites::nextButton2.height);
		}
		void update(BUTTON &buttonToCheck, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToCheck.rec)
				&& IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
				buttonToCheck.active = true;
				PlaySound(audio::buttonSfx);
			}
		}
		void updateMuteButton(BUTTON &buttonToCheck, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToCheck.rec)
				&& IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
				buttonToCheck.active = true;
				PlaySound(audio::buttonSfx);
				switch (audio::muted) {
				case true:
					audio::unmute();
					audio::muted = false;
					break;
				case false:
					audio::mute();
					audio::muted = true;
					break;
				}
			}
		}
		void draw(BUTTON buttonToDraw, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToDraw.rec)) {
				DrawTexture(sprites::normalButton2, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(WHITE, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), GREEN);
				#endif
			}
			else {
				DrawTexture(sprites::normalButton1, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(GRAY, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), DARKGREEN);
				#endif
			}
		}
		void drawNextButton(BUTTON buttonToDraw, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToDraw.rec)) {
				DrawTexture(sprites::nextButton2, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(WHITE, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), GREEN);
				#endif
			}
			else {
				DrawTexture(sprites::nextButton1, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(GRAY, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), DARKGREEN);
				#endif
			}
		}
		void drawPreviousButton(BUTTON buttonToDraw, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToDraw.rec)) {
				DrawTexture(sprites::previousButton2, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(WHITE, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), GREEN);
				#endif
			}
			else {
				DrawTexture(sprites::previousButton1, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(GRAY, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), DARKGREEN);
				#endif
			}
		}
		void drawPauseButton(BUTTON buttonToDraw, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToDraw.rec)) {
				DrawTexture(sprites::pauseButton2, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(WHITE, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), GREEN);
				#endif
			}
			else {
				DrawTexture(sprites::pauseButton1, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(GRAY, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), DARKGREEN);
				#endif
			}
		}
		void drawMuteButton(BUTTON buttonToDraw, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToDraw.rec) && !audio::muted) {
				DrawTexture(sprites::muteButton2, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(WHITE, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), GREEN);
				#endif
			}
			else {
				if (audio::muted) {
					DrawTexture(sprites::muteButton3, static_cast<int>(buttonToDraw.rec.x),
						static_cast<int>(buttonToDraw.rec.y), WHITE);
				}
				else {
					DrawTexture(sprites::muteButton1, static_cast<int>(buttonToDraw.rec.x),
						static_cast<int>(buttonToDraw.rec.y), WHITE);
				}
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(GRAY, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), DARKGREEN);
				#endif
			}
		}
		void drawSettingsButton(BUTTON buttonToDraw, Vector2 mousePosition) {
			if (CheckCollisionPointRec(mousePosition, buttonToDraw.rec)) {
				DrawTexture(sprites::settingsButton2, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(WHITE, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), GREEN);
				#endif
			}
			else {
				DrawTexture(sprites::settingsButton1, static_cast<int>(buttonToDraw.rec.x),
					static_cast<int>(buttonToDraw.rec.y), WHITE);
				#if DEBUG
				DrawRectangle(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), Fade(GRAY, 0.4f));
				DrawRectangleLines(static_cast<int>(buttonToDraw.rec.x), static_cast<int>(buttonToDraw.rec.y),
					static_cast<int>(buttonToDraw.rec.width), static_cast<int>(buttonToDraw.rec.height), DARKGREEN);
				#endif
			}
		}
	}
}