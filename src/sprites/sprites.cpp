#include "sprites.h"

namespace match3 {
	namespace sprites {

		float tileSpriteW;
		float tileSpriteH;
		float normalButtonW;
		float normalButtonH;
		float tinyButtonW = 0;
		float tinyButtonH = 0;
		Image tileImage1;
		Image tileImage2;
		Image tileImage3;
		Image tileImage4;
		Image tileImage5;
		Image normalButtonImage1;
		Image normalButtonImage2;
		Image backgroundsImages[screens::amountOfWorlds] = { 0 };
		Image playerImage;
		Image darkEnemyImage;
		Image wormEnemyImage;
		Image zombieEnemyImage;
		Image plantEnemyImage;
		Image skullEnemyImage;
		Image mosnterEnemyImage;
		Image nextButtonImage1;
		Image nextButtonImage2;
		Image previousButtonImage1;
		Image previousButtonImage2;
		Image pauseButtonImage1;
		Image pauseButtonImage2;
		Image muteButtonImage1;
		Image muteButtonImage2;
		Image muteButtonImage3;
		Image settingsButtonImage1;
		Image settingsButtonImage2;
		Texture2D nextButton1;
		Texture2D nextButton2;
		Texture2D previousButton1;
		Texture2D previousButton2;
		Texture2D tileSprite1;
		Texture2D tileSprite2;
		Texture2D tileSprite3;
		Texture2D tileSprite4;
		Texture2D tileSprite5;
		Texture2D normalButton1;
		Texture2D normalButton2;
		Texture2D backgrounds[screens::amountOfWorlds] = { 0 };
		Texture2D player;
		Texture2D darkEnemy;
		Texture2D wormEnemy;
		Texture2D zombieEnemy;
		Texture2D plantEnemy;
		Texture2D skullEnemy;
		Texture2D mosnterEnemy;
		Texture2D pauseButton1;
		Texture2D pauseButton2;
		Texture2D muteButton1;
		Texture2D muteButton2;
		Texture2D muteButton3;
		Texture2D settingsButton1;
		Texture2D settingsButton2;
		const int playerIdleFrames = 2;
		const int enemyIdleFrames = 2;
				
		void init() {
			tileSpriteW = (screens::screenWidth / 25.6f);
			tileSpriteH = (screens::screenHeight / 14.4f);
			normalButtonW = (screens::screenWidth / 6.4f);
			normalButtonH = (screens::screenHeight / 10.30f);
			tinyButtonH = (screens::screenHeight / 16.0f);
			tinyButtonW = (screens::screenWidth / 28.44f);
			tileImage1 = LoadImage("res/tiles/tiles_01.png");
			tileImage2 = LoadImage("res/tiles/tiles_02.png");
			tileImage3 = LoadImage("res/tiles/tiles_03.png");
			tileImage4 = LoadImage("res/tiles/tiles_04.png");
			tileImage5 = LoadImage("res/tiles/tiles_05.png");
			normalButtonImage1 = LoadImage("res/buttons/normal_button_01.png");
			normalButtonImage2 = LoadImage("res/buttons/normal_button_02.png");
			playerImage = LoadImage("res/characters/knight/knight.png");
			darkEnemyImage = LoadImage("res/characters/dark/dark.png");
			wormEnemyImage = LoadImage("res/characters/worm/worm.png");
			zombieEnemyImage = LoadImage("res/characters/zombie/zombie.png");
			plantEnemyImage = LoadImage("res/characters/plant/plant.png");
			skullEnemyImage = LoadImage("res/characters/skull/skull.png");
			mosnterEnemyImage = LoadImage("res/characters/monster/monster.png");
			nextButtonImage1 = LoadImage("res/buttons/next_button_01.png");
			nextButtonImage2 = LoadImage("res/buttons/next_button_02.png");
			previousButtonImage1 = LoadImage("res/buttons/previous_button_01.png");
			previousButtonImage2 = LoadImage("res/buttons/previous_button_02.png");
			pauseButtonImage1 = LoadImage("res/buttons/pause_button_01.png");
			pauseButtonImage2 = LoadImage("res/buttons/pause_button_02.png");
			muteButtonImage1 = LoadImage("res/buttons/muted_sound_button_01.png");
			muteButtonImage2 = LoadImage("res/buttons/muted_sound_button_02.png");
			muteButtonImage3 = LoadImage("res/buttons/muted_sound_button_03.png");
			settingsButtonImage1 = LoadImage("res/buttons/settings_button_01.png");
			settingsButtonImage2 = LoadImage("res/buttons/settings_button_02.png");
			ImageResize(&tileImage1, static_cast<int>(tileSpriteW), static_cast<int>(tileSpriteH));
			ImageResize(&tileImage2, static_cast<int>(tileSpriteW), static_cast<int>(tileSpriteH));
			ImageResize(&tileImage3, static_cast<int>(tileSpriteW), static_cast<int>(tileSpriteH));
			ImageResize(&tileImage4, static_cast<int>(tileSpriteW), static_cast<int>(tileSpriteH));
			ImageResize(&tileImage5, static_cast<int>(tileSpriteW), static_cast<int>(tileSpriteH));
			ImageResize(&normalButtonImage1, static_cast<int>(normalButtonW), static_cast<int>(normalButtonH));
			ImageResize(&normalButtonImage2, static_cast<int>(normalButtonW), static_cast<int>(normalButtonH));
			ImageResize(&playerImage, static_cast<int>(screens::screenWidth / 2*playerIdleFrames),
				static_cast<int>(screens::screenWidth / 3.6f));
			ImageResize(&darkEnemyImage, static_cast<int>(screens::screenWidth / 8.0f*enemyIdleFrames),
				static_cast<int>(screens::screenHeight / 5.7f));
			ImageResize(&wormEnemyImage, static_cast<int>(screens::screenWidth / 8.0f*enemyIdleFrames),
				static_cast<int>(screens::screenHeight / 4.6f));
			ImageResize(&zombieEnemyImage, static_cast<int>(screens::screenWidth / 8.0f*enemyIdleFrames),
				static_cast<int>(screens::screenHeight / 4.1f));
			ImageResize(&plantEnemyImage, static_cast<int>(screens::screenWidth / 8.0f*enemyIdleFrames),
				static_cast<int>(screens::screenHeight / 3.38f));
			ImageResize(&skullEnemyImage, static_cast<int>(screens::screenWidth / 8.0f*enemyIdleFrames),
				static_cast<int>(screens::screenHeight / 4.91f));
			ImageResize(&mosnterEnemyImage, static_cast<int>(screens::screenWidth / 8.0f*enemyIdleFrames),
				static_cast<int>(screens::screenHeight / 3.79f));
			ImageResize(&nextButtonImage1, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&nextButtonImage2, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&previousButtonImage1, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&previousButtonImage2, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&pauseButtonImage1, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&pauseButtonImage2, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&muteButtonImage1, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&muteButtonImage2, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&muteButtonImage3, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&settingsButtonImage1, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			ImageResize(&settingsButtonImage2, static_cast<int>(tinyButtonW), static_cast<int>(tinyButtonH));
			tileSprite1 = LoadTextureFromImage(tileImage1);
			tileSprite2 = LoadTextureFromImage(tileImage2);
			tileSprite3 = LoadTextureFromImage(tileImage3);
			tileSprite4 = LoadTextureFromImage(tileImage4);
			tileSprite5 = LoadTextureFromImage(tileImage5);
			normalButton1 = LoadTextureFromImage(normalButtonImage1);
			normalButton2 = LoadTextureFromImage(normalButtonImage2);
			player = LoadTextureFromImage(playerImage);
			darkEnemy = LoadTextureFromImage(darkEnemyImage);
			wormEnemy = LoadTextureFromImage(wormEnemyImage);
			zombieEnemy = LoadTextureFromImage(zombieEnemyImage);
			plantEnemy = LoadTextureFromImage(plantEnemyImage);
			skullEnemy = LoadTextureFromImage(skullEnemyImage);
			mosnterEnemy = LoadTextureFromImage(mosnterEnemyImage);
			nextButton1 = LoadTextureFromImage(nextButtonImage1);
			nextButton2 = LoadTextureFromImage(nextButtonImage2);
			previousButton1 = LoadTextureFromImage(previousButtonImage1);
			previousButton2 = LoadTextureFromImage(previousButtonImage2);
			pauseButton1 = LoadTextureFromImage(pauseButtonImage1);
			pauseButton2 = LoadTextureFromImage(pauseButtonImage2);
			muteButton1 = LoadTextureFromImage(muteButtonImage1);
			muteButton2 = LoadTextureFromImage(muteButtonImage2);
			muteButton3 = LoadTextureFromImage(muteButtonImage3);
			settingsButton1 = LoadTextureFromImage(settingsButtonImage1);
			settingsButton2 = LoadTextureFromImage(settingsButtonImage2);
			backgroundsImages[0] = LoadImage("res/backgrounds/background_1.png");
			backgroundsImages[1] = LoadImage("res/backgrounds/background_2.png");
			backgroundsImages[2] = LoadImage("res/backgrounds/background_3.png");
			backgroundsImages[3] = LoadImage("res/backgrounds/background_4.png");
			backgroundsImages[4] = LoadImage("res/backgrounds/background_5.png");
			backgroundsImages[5] = LoadImage("res/backgrounds/background_6.png");
			for (int i = 0; i < screens::amountOfWorlds; i++) {
				ImageResize(&backgroundsImages[i], static_cast<int>(screens::screenWidth),
					static_cast<int>(screens::screenHeight));
				backgrounds[i] = LoadTextureFromImage(backgroundsImages[i]);
			}
		}
		void drawStaticBackground() {
			DrawTexture(backgrounds[0], screens::screenZero, screens::screenZero, WHITE);
			DrawRectangle(screens::screenZero, screens::screenZero, screens::screenWidth,
				screens::screenHeight, Fade(BLACK, 0.2f));
		}
		void deinit() {
			UnloadImage(tileImage1);
			UnloadImage(tileImage2);
			UnloadImage(tileImage3);
			UnloadImage(tileImage4);
			UnloadImage(tileImage5);
			UnloadImage(normalButtonImage1);
			UnloadImage(normalButtonImage2);
			UnloadImage(playerImage);
			UnloadImage(darkEnemyImage);
			UnloadImage(wormEnemyImage);
			UnloadImage(zombieEnemyImage);
			UnloadImage(plantEnemyImage);
			UnloadImage(skullEnemyImage);
			UnloadImage(mosnterEnemyImage);
			UnloadImage(nextButtonImage1);
			UnloadImage(nextButtonImage2);
			UnloadImage(previousButtonImage1);
			UnloadImage(previousButtonImage2);
			UnloadImage(pauseButtonImage1);
			UnloadImage(pauseButtonImage2);
			UnloadImage(muteButtonImage1);
			UnloadImage(muteButtonImage2);
			UnloadImage(muteButtonImage3);
			UnloadImage(settingsButtonImage1);
			UnloadImage(settingsButtonImage2);
			UnloadTexture(tileSprite1);
			UnloadTexture(tileSprite2);
			UnloadTexture(tileSprite3);
			UnloadTexture(tileSprite4);
			UnloadTexture(tileSprite5);
			UnloadTexture(normalButton1);
			UnloadTexture(normalButton2);
			UnloadTexture(player);
			UnloadTexture(darkEnemy);
			UnloadTexture(wormEnemy);
			UnloadTexture(zombieEnemy);
			UnloadTexture(plantEnemy);
			UnloadTexture(skullEnemy);
			UnloadTexture(mosnterEnemy);
			UnloadTexture(nextButton1);
			UnloadTexture(nextButton2);
			UnloadTexture(previousButton1);
			UnloadTexture(previousButton2);
			UnloadTexture(pauseButton1);
			UnloadTexture(pauseButton2);
			UnloadTexture(muteButton1);
			UnloadTexture(muteButton2);
			UnloadTexture(muteButton3);
			UnloadTexture(settingsButton1);
			UnloadTexture(settingsButton2);
			for (int i = 0; i < screens::amountOfWorlds; i++) {
				UnloadImage(backgroundsImages[i]);
			}
			for (int i = 0; i < screens::amountOfWorlds; i++) {
				UnloadTexture(backgrounds[i]);
			}
		}
	}
}