#ifndef	SPRITES_H
#define SPRITES_H
#include "raylib.h"
#include "screens_manager/screens.h"

namespace match3 {
	namespace sprites {

		extern Texture2D tileSprite1;
		extern Texture2D tileSprite2;
		extern Texture2D tileSprite3;
		extern Texture2D tileSprite4;
		extern Texture2D tileSprite5;
		extern Texture2D normalButton1;
		extern Texture2D normalButton2;
		extern Texture2D backgrounds[screens::amountOfWorlds];
		extern Texture2D player;
		extern Texture2D darkEnemy;
		extern Texture2D wormEnemy;
		extern Texture2D zombieEnemy;
		extern Texture2D plantEnemy;
		extern Texture2D skullEnemy;
		extern Texture2D mosnterEnemy;
		extern Texture2D nextButton1;
		extern Texture2D nextButton2;
		extern Texture2D previousButton1;
		extern Texture2D previousButton2;
		extern Texture2D pauseButton1;
		extern Texture2D pauseButton2;
		extern Texture2D muteButton1;
		extern Texture2D muteButton2;
		extern Texture2D muteButton3;
		extern Texture2D settingsButton1;
		extern Texture2D settingsButton2;
		extern int backgroundIndex;

		void init();
		void drawStaticBackground();
		void deinit();
	}
}
#endif