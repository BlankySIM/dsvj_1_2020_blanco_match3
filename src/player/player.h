#ifndef PLAYER_H
#define	PLAYER_H
#include "raylib.h"
#include "tile/tile.h"
#include "screens_manager/screens.h"
#include "sprites/sprites.h"
#include "audio/audio.h"

using namespace std;

namespace match3 {
	namespace player {

		const int totalElements = 5;
		struct ELEMENT_COUNTER {
			int type;
			int amount;
		};
		struct PLAYER {
			Vector2 mousePosition;
			float damage;
			float attackBarSpeed;
			int enemiesDefeated;
			Rectangle lifeBar;
			Rectangle attackBar;
			int selectedTileType;
			ELEMENT_COUNTER counter[totalElements];
			int attackType;
			Texture2D attackSprite;
			bool alive;
			int currentLevel;
			int score;
			int highestScore;
			bool attacking;
			bool selecting;
		};

		void init();
		void inputs();
		void update(Rectangle &enemyLifeBar, int enemyWeakness, int enemyResistance);
		void draw(bool enemyAttacking);
		void reinit();
		void resize();
		extern PLAYER matchPlayer;
	}
}
#endif