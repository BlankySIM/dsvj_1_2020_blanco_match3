#include "player.h"

namespace match3 {
	namespace player {

		enum Element {
			redType,
			greenType,
			violetType,
			blueType,
			yellowType
		};
		int key;
		bool destroying = false;
		tile::TILE currentTile;
		int tilesSelected = 0;
		int previousI;
		int previousJ;
		const int minMatch = 3;
		float maxAttackBar;
		float maxLifeBar;
		const int idleFrames = 2;
		int framesCounter = 0;
		int frameSpeed = 2;
		int currentFrame = 0;
		Vector2 tilesList[tile::maxArrayH * tile::maxArrayW];
		Rectangle frameRec;
		Vector2 playerSpritePos;
		bool animateBar = false;
		const float barMaxCounter = 100.0f;
		float barAnimationCounter = barMaxCounter;
		const float counterDecreaser = 100.f;
		float animationOffset = 0;
		Color animationColor = BLACK;
		const float attackMaxCounter = 100.0f;
		float attackAnimationCounter = attackMaxCounter;
		const float maxInvisibleTime = 20.0f;
		bool invisible = false;
		float invisivilityCounter = maxInvisibleTime;

		static void invisibleFrames() {
			invisivilityCounter -= counterDecreaser * GetFrameTime();
			if (invisivilityCounter <= 0) {
				invisivilityCounter = maxInvisibleTime;
				invisible = !invisible;
			}
		}
		static void playerAttackAnimation() {
			if (matchPlayer.attacking) {
				attackAnimationCounter -= counterDecreaser * GetFrameTime();
				if (attackAnimationCounter <= 0) {
					attackAnimationCounter = attackMaxCounter;
					matchPlayer.attacking = false;
				}
			}
		}
		static void setNewHighScore() {
			if (matchPlayer.score > matchPlayer.highestScore) {
				matchPlayer.highestScore = matchPlayer.score;
			}
		}
		static void addScore(int enemyWeakness, int enemyResistance, int selectedTilesType) {
			if (selectedTilesType == enemyWeakness) {
				matchPlayer.score += tilesSelected + (((tilesSelected*tilesSelected) / 2) * 2);
			}
			else if (selectedTilesType == enemyResistance) {
				matchPlayer.score += tilesSelected + (((tilesSelected*tilesSelected) / 2) / 2);
			}
			else {
				matchPlayer.score += tilesSelected + ((tilesSelected*tilesSelected) / 2);
			}
			if (matchPlayer.score > 999999999) {
				matchPlayer.score = 999999999;
			}
		}
		static void attackBarAnimation() {
			if (animateBar) {				
				barAnimationCounter -= counterDecreaser * GetFrameTime();
				if (barAnimationCounter <= 0) {
					barAnimationCounter = barMaxCounter;
					animateBar = false;
				}
			}
		}
		static void updatePlayerFrames()
		{
			framesCounter++;

			if (framesCounter >= (screens::targetFPS / frameSpeed))
			{
				framesCounter = 0;
				currentFrame++;
				if (currentFrame >= idleFrames) currentFrame = 0;

				frameRec.x = static_cast<float>(currentFrame * (sprites::player.width / idleFrames));
			}
		}
		static void setPlayerDeath() {
			if (matchPlayer.lifeBar.width <= 0) {
				matchPlayer.alive = false;
			}
		}
		static void resetElementCounter() {
			for (int i = 0; i < totalElements; i++) {
				matchPlayer.counter[i].amount = 0;
			}
		}
		static void attack(Rectangle &enemyLifeBar, int enemyWeakness, int enemyResistance, int selectedTilesType) {
			if (selectedTilesType == enemyWeakness) {
				matchPlayer.attackBar.width += matchPlayer.attackBarSpeed*(tilesSelected*2);
			}
			else if (selectedTilesType == enemyResistance) {
				matchPlayer.attackBar.width += matchPlayer.attackBarSpeed*(tilesSelected/2);
			}
			else{
				matchPlayer.attackBar.width += matchPlayer.attackBarSpeed*tilesSelected;
			}
			if (matchPlayer.attackBar.width >= maxAttackBar) {
				attackAnimationCounter = attackMaxCounter;
				matchPlayer.attacking = true;
				matchPlayer.attackBar.width = 0;
				if (matchPlayer.attackType == enemyWeakness) {
					matchPlayer.score += tilesSelected + (((tilesSelected*tilesSelected) / 2)*2);
					PlaySound(audio::attackSfx);
					PlaySound(audio::poweredAttackSfx);
					enemyLifeBar.width -= static_cast<int>(matchPlayer.damage * 2.5);
				}
				else if (matchPlayer.attackType == enemyResistance) {
					matchPlayer.score += tilesSelected + (((tilesSelected*tilesSelected) / 2) / 2);
					PlaySound(audio::weakAttackSfx);
					enemyLifeBar.width -= matchPlayer.damage / 2;
				}
				else {
					matchPlayer.score += tilesSelected + ((tilesSelected*tilesSelected) / 2);
					PlaySound(audio::attackSfx);
					enemyLifeBar.width -= matchPlayer.damage;
				}
				resetElementCounter();
			}
		}
		static void setPlayerAttack() {
			ELEMENT_COUNTER auxPosition;
			for (int j = 0; j < totalElements; j++)
			{
				for (int i = 0; i < totalElements - 1; i++) {
					if (matchPlayer.counter[i].amount < matchPlayer.counter[i + 1].amount) {
						auxPosition = matchPlayer.counter[i];
						matchPlayer.counter[i] = matchPlayer.counter[i + 1];
						matchPlayer.counter[i + 1] = auxPosition;
					}
				}			
			}
			switch (matchPlayer.counter[0].type)
			{
			case redType:
				matchPlayer.attackSprite = sprites::tileSprite1;				
				break;
			case greenType:
				matchPlayer.attackSprite = sprites::tileSprite2;
				break;
			case violetType:
				matchPlayer.attackSprite = sprites::tileSprite3;
				break;
			case blueType:
				matchPlayer.attackSprite = sprites::tileSprite4;
				break;
			case yellowType:
				matchPlayer.attackSprite = sprites::tileSprite5;
				break;
			}
			matchPlayer.attackType = matchPlayer.counter[0].type;
		}
		static void selectTile(int i, int j) {
			tile::auxGrid[i][j].selected = true;
			currentTile = tile::auxGrid[i][j];
			tilesList[tilesSelected].x = tile::auxGrid[i][j].rec.x;
			tilesList[tilesSelected].y = tile::auxGrid[i][j].rec.y;
			tilesSelected++;
			previousI = i;
			previousJ = j;
			tile::grid[i][j] = tile::auxGrid[i][j];
		}
		static bool isInList(tile::TILE selectedTile) {
			for (int i = 0; i < tilesSelected; i++) {
				if (selectedTile.rec.x == tilesList[i].x && selectedTile.rec.y == tilesList[i].y) {
					return true;
				}
			}
			return false;
		}
		static void returnToPrevious(tile::TILE selectedTile) {
			int repeatedTile;
			int auxTilesSelected = tilesSelected;
			for (int i = 0; i < auxTilesSelected; i++) {
				if (selectedTile.rec.x == tilesList[i].x && selectedTile.rec.y == tilesList[i].y) {
					repeatedTile = i;
				}
			}			
			for (int x = auxTilesSelected -1; x > repeatedTile; x--) {
				for (int i = 0; i < tile::maxArrayW; i++) {
					for (int j = 0; j < tile::maxArrayH; j++) {
						if (tile::auxGrid[i][j].rec.x == tilesList[x].x && tile::auxGrid[i][j].rec.y == tilesList[x].y) {
									tile::auxGrid[i][j].selected = false;
									tile::grid[i][j].selected = tile::auxGrid[i][j].selected;						
									tilesSelected--;									
						}
					}
				}
			}
		}
		void init() {
			matchPlayer.damage = static_cast<float>(screens::screenWidth / 45);
			matchPlayer.attackBarSpeed = static_cast<float>(screens::screenWidth / 180);
			matchPlayer.enemiesDefeated = 0;
			maxAttackBar = screens::screenWidth / 6.4f;
			maxLifeBar = maxAttackBar;
			matchPlayer.attackBar.height = screens::screenHeight / 24.0f;
			matchPlayer.attackBar.width = 0.0f;
			matchPlayer.attackBar.x = screens::screenZero + screens::screenWidth / 12.0f;
			matchPlayer.attackBar.y = screens::middleScreenH - matchPlayer.attackBar.height - screens::screenHeight / 9;
			matchPlayer.lifeBar.height = screens::screenHeight / 24.0f;
			matchPlayer.lifeBar.width = maxLifeBar;
			matchPlayer.lifeBar.x = screens::screenZero + screens::screenWidth / 12.0f;
			matchPlayer.lifeBar.y = screens::middleScreenH + matchPlayer.attackBar.height + screens::screenHeight / 4;
			matchPlayer.selectedTileType = 0;
			for (int i = 0; i < totalElements; i++) {
				matchPlayer.counter[i].type = i;
				matchPlayer.counter[i].amount = 0;
			}
			matchPlayer.attackType = 0;
			matchPlayer.attackSprite = sprites::tileSprite1;
			matchPlayer.alive = true;
			matchPlayer.enemiesDefeated = 0;
			matchPlayer.currentLevel = 1;
			matchPlayer.score = 0;
			matchPlayer.highestScore = 0;
			frameRec.width = static_cast<float>(sprites::player.width / idleFrames);
			frameRec.height = static_cast<float>(sprites::player.height);
			playerSpritePos.x = matchPlayer.attackBar.x - screens::screenWidth / 5.2f;
			playerSpritePos.y = matchPlayer.attackBar.y - screens::screenHeight / 19.0f;
			matchPlayer.attacking = false;
			matchPlayer.selecting = false;
			animationOffset = screens::screenWidth / 200.0f;
		}
		void inputs() {
			matchPlayer.mousePosition = GetMousePosition();
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) {
				matchPlayer.selecting = true;
			}
			else if (IsMouseButtonReleased(MOUSE_LEFT_BUTTON)) {
				destroying = true;
			}
			else {
				matchPlayer.selecting = false;
				destroying = false;
			}
		}
		void update(Rectangle &enemyLifeBar, int enemyWeakness, int enemyResistance) {
			updatePlayerFrames();			
			for (int i = 0; i < tile::maxArrayW; i++) {
				for (int j = 0; j < tile::maxArrayH; j++) {
					if (matchPlayer.selecting  && CheckCollisionPointRec(matchPlayer.mousePosition, tile::auxGrid[i][j].rec)
						&& tile::auxGrid[i][j].active && !destroying) {
						if (tilesSelected == 0) {
							selectTile(i, j);
							matchPlayer.selectedTileType = tile::auxGrid[i][j].type;							
						}
						if (tile::auxGrid[i][j].type == currentTile.type && tile::checkMatch(previousI, previousJ, i, j)
							&& !tile::auxGrid[i][j].selected) {
							selectTile(i, j);
						}
						if (tile::auxGrid[i][j].type == currentTile.type && tile::auxGrid[i][j].selected
							&& isInList(tile::auxGrid[i][j])) {
							returnToPrevious(tile::auxGrid[i][j]);
							currentTile = tile::auxGrid[i][j];
							previousI = i;
							previousJ = j;
							tile::grid[i][j] = tile::auxGrid[i][j];
						}
					}
				}
			}
			if (destroying) {
				if (tilesSelected >= minMatch) {
					PlaySound(audio::matchSfx);
					addScore(enemyWeakness, enemyResistance, currentTile.type);
					animationColor = currentTile.colorType;
					barAnimationCounter = barMaxCounter;
					animateBar = true;
					tile::destroySelected();
					for (int i = 0; i < totalElements; i++) {
						if (matchPlayer.selectedTileType == matchPlayer.counter[i].type) {
							matchPlayer.counter[i].amount += tilesSelected;
						}
					}
				setPlayerAttack();
				attack(enemyLifeBar, enemyWeakness, enemyResistance, currentTile.type);
				}
				else {
					tile::unselect();
				}				
				tilesSelected = 0;
			}
			setPlayerDeath();
			attackBarAnimation();
			playerAttackAnimation();
			setNewHighScore();
		}
		void draw(bool enemyAttacking) {
			DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero + screens::screenHeight / 15),
				static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenZero + screens::screenHeight / 15),
				Fade(BLACK, 0.5f));
			DrawRectangle(static_cast<int>(matchPlayer.attackBar.x - screens::screenWidth / 40),
				static_cast<int>(matchPlayer.attackBar.y - screens::screenHeight / 5),
				static_cast<int>(screens::screenWidth / 4.8f), static_cast<int>(screens::screenHeight / 1.4f), Fade(BLACK, 0.8f));
			if (animateBar) {
				DrawRectangle(static_cast<int>((matchPlayer.attackBar.x)-animationOffset),
					static_cast<int>((matchPlayer.attackBar.y)-animationOffset),
					static_cast<int>((maxAttackBar)+(animationOffset*2)),
					static_cast<int>((matchPlayer.attackBar.height)+(animationOffset*2)), animationColor);
			}
			DrawRectangle(static_cast<int>(matchPlayer.attackBar.x), static_cast<int>(matchPlayer.attackBar.y),
				static_cast<int>(maxAttackBar), static_cast<int>(matchPlayer.attackBar.height), WHITE);
			DrawRectangle(static_cast<int>(matchPlayer.lifeBar.x), static_cast<int>(matchPlayer.lifeBar.y),
				static_cast<int>(maxLifeBar), static_cast<int>(matchPlayer.lifeBar.height), WHITE);
			DrawRectangle(static_cast<int>(matchPlayer.attackBar.x), static_cast<int>(matchPlayer.attackBar.y),
				static_cast<int>(matchPlayer.attackBar.width), static_cast<int>(matchPlayer.attackBar.height), BLUE);
			DrawRectangle(static_cast<int>(matchPlayer.lifeBar.x), static_cast<int>(matchPlayer.lifeBar.y),
				static_cast<int>(matchPlayer.lifeBar.width), static_cast<int>(matchPlayer.lifeBar.height), RED);
			DrawRectangleLines(static_cast<int>(matchPlayer.attackBar.x), static_cast<int>(matchPlayer.attackBar.y),
				static_cast<int>(maxAttackBar), static_cast<int>(matchPlayer.attackBar.height), BLACK);
			DrawRectangleLines(static_cast<int>(matchPlayer.lifeBar.x), static_cast<int>(matchPlayer.lifeBar.y),
				static_cast<int>(maxLifeBar), static_cast<int>(matchPlayer.lifeBar.height), BLACK);
			DrawTexture(matchPlayer.attackSprite, static_cast<int>(matchPlayer.attackBar.x),
				static_cast<int>(matchPlayer.attackBar.y - screens::screenHeight / 7), WHITE);
			DrawText("->Next Atq", static_cast<int>(matchPlayer.attackBar.x + matchPlayer.attackSprite.width),
				static_cast<int>((matchPlayer.attackBar.y - screens::screenHeight / 7) +
				(matchPlayer.attackSprite.height / 2 - screens::medFontSize / 2)), screens::medFontSize, WHITE);
			DrawText(FormatText("Score: %0i", matchPlayer.score),
				static_cast<int>(screens::screenZero + (screens::screenWidth / 100)),
				static_cast<int>(screens::screenZero + screens::screenHeight / 12.5f), screens::medFontSize, WHITE);
			if (enemyAttacking) {
				invisibleFrames();
				if (!invisible) {
					DrawTextureRec(sprites::player, frameRec, playerSpritePos, RED);
				}
			}
			else {
				DrawTextureRec(sprites::player, frameRec, playerSpritePos, WHITE);
			}
		}
		void reinit() {
			matchPlayer.attackBar.width = 0;
			matchPlayer.lifeBar.width = maxLifeBar;
			resetElementCounter();
			matchPlayer.alive = true;
			matchPlayer.attacking = false;
			attackAnimationCounter = attackMaxCounter;
			animateBar = false;
			currentFrame = 0;
			frameRec.x = 0;
		}
		void resize() {
			matchPlayer.damage = static_cast<float>(screens::screenWidth / 45);
			matchPlayer.attackBarSpeed = static_cast<float>(screens::screenWidth / 180);
			maxAttackBar = screens::screenWidth / 6.4f;
			maxLifeBar = maxAttackBar;
			matchPlayer.attackBar.height = screens::screenHeight / 24.0f;
			matchPlayer.attackBar.width = 0.0f;
			matchPlayer.attackBar.x = screens::screenZero + screens::screenWidth / 12.0f;
			matchPlayer.attackBar.y = screens::middleScreenH - matchPlayer.attackBar.height - screens::screenHeight / 9;
			matchPlayer.lifeBar.height = screens::screenHeight / 24.0f;
			matchPlayer.lifeBar.width = maxLifeBar;
			matchPlayer.lifeBar.x = screens::screenZero + screens::screenWidth / 12.0f;
			matchPlayer.lifeBar.y = screens::middleScreenH + matchPlayer.attackBar.height + screens::screenHeight / 4;
			matchPlayer.attackType = 0;
			matchPlayer.attackSprite = sprites::tileSprite1;
			frameRec.width = static_cast<float>(sprites::player.width / idleFrames);
			frameRec.height = static_cast<float>(sprites::player.height);
			playerSpritePos.x = matchPlayer.attackBar.x - screens::screenWidth / 5.2f;
			playerSpritePos.y = matchPlayer.attackBar.y - screens::screenHeight / 19.0f;
			animationOffset = screens::screenWidth / 200.0f;
			currentFrame = 0;
			frameRec.x = 0;
		}
		PLAYER matchPlayer = { 0 };
	}
}
