#include "tile.h"

namespace match3 {
	namespace tile {

		enum Type{
			type1,
			type2,
			type3,
			type4,
			type5,
		};
		float tileW;
		float tileH;
		float tileContinerW;
		float tileContainerH;
		float tilesOffsetX;
		float tilesOffsetY;
		float selectedTileAlpha = 1.0f;
		const float alphaModifier = 2.0f;
		bool fadeIn = false;
		bool fadeOut = true;

		static void tileFall() {
			TILE auxTile;
			for (int x = 0; x < maxArrayH - 1; x++) {
				for (int i = 0; i < maxArrayW; i++) {
					for (int j = maxArrayH - 1; j > 0; j--) {
						if (!tile::auxGrid[i][j].active) {
							auxTile.sprite = auxGrid[i][j].sprite;
							auxGrid[i][j].sprite = auxGrid[i][j - 1].sprite;
							auxGrid[i][j - 1].sprite = auxTile.sprite;
							auxTile.colorType = auxGrid[i][j].colorType;
							auxGrid[i][j].colorType = auxGrid[i][j - 1].colorType;
							auxGrid[i][j - 1].colorType = auxTile.colorType;
							auxTile.type = auxGrid[i][j].type;
							auxGrid[i][j].type = auxGrid[i][j - 1].type;
							auxGrid[i][j - 1].type = auxTile.type;
							auxTile.active = auxGrid[i][j].active;
							auxGrid[i][j].active = auxGrid[i][j - 1].active;
							auxGrid[i][j - 1].active = auxTile.active;
							grid[i][j] = auxGrid[i][j];
							grid[i][j - 1] = auxGrid[i][j - 1];
						}
					}
				}
			}
		}
		static void generateRandomType(TILE selectedGrid[maxArrayW][maxArrayH],int j, int i) {
			int randType = 0;
			randType = GetRandomValue(type1, type5);
			selectedGrid[i][j].type = randType;
			switch (selectedGrid[i][j].type) {
			case type1:
				selectedGrid[i][j].colorType = RED;
				selectedGrid[i][j].sprite = sprites::tileSprite1;
				break;
			case type2:
				selectedGrid[i][j].colorType = GREEN;
				selectedGrid[i][j].sprite = sprites::tileSprite2;
				break;
			case type3:
				selectedGrid[i][j].colorType = VIOLET;
				selectedGrid[i][j].sprite = sprites::tileSprite3;
				break;
			case type4:
				selectedGrid[i][j].colorType = BLUE;
				selectedGrid[i][j].sprite = sprites::tileSprite4;
				break;
			case type5:
				selectedGrid[i][j].colorType = YELLOW;
				selectedGrid[i][j].sprite = sprites::tileSprite5;
				break;
			}
		}
		static void regenerateDestroyed() {
			for (int i = 0; i < maxArrayW; i++) {
				for (int j = 0; j < maxArrayH; j++) {
					if (!auxGrid[i][j].active) {
						generateRandomType(auxGrid, j, i);
						auxGrid[i][j].selected = false;
						auxGrid[i][j].active = true;
						grid[i][j] = auxGrid[i][j];
					}
				}
			}
		}
		void init() {
			tileW = static_cast<float>(sprites::tileSprite1.width);
			tileH = static_cast<float>(sprites::tileSprite1.height);
			tileContinerW = (screens::screenWidth / 23.7f);
			tileContainerH = (screens::screenHeight / 13.3f);
			tilesOffsetX = (tileContinerW - tileW) / 2;
			tilesOffsetY = (tileContainerH - tileH) / 2;
			int initialPosx = static_cast<int>(screens::middleScreenW - ((maxArrayW / 2.0f) * tileContinerW));
			int initialPosy = static_cast<int>(screens::screenZero + (screens::screenHeight / 3.0f));

			for (int i = 0; i < maxArrayW; i++) {
				for (int j = 0; j < maxArrayH; j++) {
					grid[i][j].rec.x = initialPosx + (i * tileContinerW) + tilesOffsetX;
					grid[i][j].rec.y = initialPosy + (j * tileContainerH) + tilesOffsetY;
					grid[i][j].rec.width = tileW;
					grid[i][j].rec.height = tileH;
					grid[i][j].active = true;
					grid[i][j].selected = false;
					generateRandomType(grid, j, i);
					auxGrid[i][j] = grid[i][j];
				}
			}			
		}
		bool checkMatch(int previousI, int previousJ, int currentI, int currentJ) {
			bool match = false;
			for (int i = previousI - 1; i < previousI + 2; i++) {
				for (int j = previousJ - 1; j < previousJ + 2; j++) {
					if (i == currentI && j == currentJ) {
						match = true;
					}
				}
			}
			return match;
		}
		void destroySelected() {
			for (int i = 0; i < maxArrayW; i++) {
				for (int j = 0; j < maxArrayH; j++) {
					if (auxGrid[i][j].selected) {
						auxGrid[i][j].selected = false;
						auxGrid[i][j].active = false;
						grid[i][j] = auxGrid[i][j];
					}
				}
			}
			tileFall();
			regenerateDestroyed();
		}
		void unselect() {
			for (int i = 0; i < maxArrayW; i++) {
				for (int j = 0; j < maxArrayH; j++) {
					if (auxGrid[i][j].selected) {
						auxGrid[i][j].selected = false;
						grid[i][j] = auxGrid[i][j];
					}
				}
			}
		}
		static void fadeAnimation() {
			if (fadeIn) {
				selectedTileAlpha -= alphaModifier * GetFrameTime();
				if (selectedTileAlpha <= 0) {
					fadeIn = false;
					fadeOut = true;
				}
			}
			else if (fadeOut) {
				selectedTileAlpha += alphaModifier * GetFrameTime();
				if (selectedTileAlpha >= 1) {
					fadeIn = true;
					fadeOut = false;
				}
			}
		}
		void draw(bool playerSelecting) {
			if (playerSelecting) {
				fadeAnimation();
			}
			DrawRectangle(static_cast<int>(grid[0][0].rec.x - tilesOffsetX),
				static_cast<int>(grid[0][0].rec.y - tilesOffsetY),
				static_cast<int>(grid[maxArrayW - 1][maxArrayH - 1].rec.x + grid[maxArrayW - 1][maxArrayH - 1].rec.width
					+ (tilesOffsetX * 2) - grid[0][0].rec.x),
				static_cast<int>(grid[maxArrayW - 1][maxArrayW - 1].rec.y + grid[maxArrayW - 1][maxArrayH - 1].rec.height
					+ (tilesOffsetY * 2) - grid[0][0].rec.y), Fade(BLACK, 0.8f));
			for (int i = 0; i < maxArrayW; i++) {
				for (int j = 0; j < maxArrayH; j++) {
					if (grid[i][j].active) {
					#if DEBUG
					DrawRectangle(static_cast<int>(grid[i][j].rec.x), static_cast<int>(grid[i][j].rec.y),
						static_cast<int>(grid[i][j].rec.width), static_cast<int>(grid[i][j].rec.height),
						Fade(grid[i][j].colorType, 0.6f));
					#endif
					DrawTexture(grid[i][j].sprite, static_cast<int>(grid[i][j].rec.x),
						static_cast<int>(grid[i][j].rec.y), WHITE);
					}
					if (grid[i][j].selected){
						DrawTexture(grid[i][j].sprite, static_cast<int>(grid[i][j].rec.x),
							static_cast<int>(grid[i][j].rec.y), Fade(BLACK, selectedTileAlpha));
					}
				}
			}
		}
		void reinitGrid() {
			for (int i = 0; i < maxArrayW; i++) {
				for (int j = 0; j < maxArrayH; j++) {
					auxGrid[i][j].active = true;
					auxGrid[i][j].selected = false;
					generateRandomType(auxGrid, j, i);
					grid[i][j] = auxGrid[i][j];
				}
			}
		}

		TILE grid[maxArrayW][maxArrayH];
		TILE auxGrid[maxArrayW][maxArrayH];
	}
}