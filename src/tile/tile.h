#ifndef TILE_H
#define TILE_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "sprites/sprites.h"

namespace match3 {
	namespace tile {

		const int maxArrayW = 8;
		const int maxArrayH = 8;
		struct TILE {		
			Rectangle rec;
			int type;
			bool active;
			Color colorType;
			bool selected;
			Texture2D sprite;
		};

		void init();
		bool checkMatch(int previousI, int previousJ, int currentI, int currentJ);
		void destroySelected();
		void unselect();
		void draw(bool playerSelecting);
		void reinitGrid();

		extern TILE grid[maxArrayW][maxArrayH];
		extern TILE auxGrid[maxArrayW][maxArrayH];
	}
}
#endif
