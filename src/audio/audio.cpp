#include "audio.h"

namespace match3 {
	namespace audio {

		Music gameplayTracks[screens::amountOfWorlds] = { 0 };
		Music menuMusic;
		Sound buttonSfx;
		Sound attackSfx;
		Sound weakAttackSfx;
		Sound poweredAttackSfx;
		Sound matchSfx;
		Sound enemyHitSfx;
		float musicVolume[screens::amountOfWorlds] = { 0 };
		float menuMusicVolume = 0.1f;
		float buttonVolume = 0.2f;
		float attackVolume = 0.3f;
		float weakAttackVolume = 0.6f;
		float poweredAttackVolume = 0.6f;
		float matchVolume = 0.3f;
		float enemyHitVolume = 1.0f;
		float mutedVolume = 0.0f;
		bool muted = false;

		void init() {
			gameplayTracks[0] = LoadMusicStream("res/music/level_music_01.mp3");
			gameplayTracks[1] = LoadMusicStream("res/music/level_music_02.mp3");
			gameplayTracks[2] = LoadMusicStream("res/music/level_music_03.mp3");
			gameplayTracks[3] = LoadMusicStream("res/music/level_music_04.mp3");
			gameplayTracks[4] = LoadMusicStream("res/music/level_music_05.mp3");
			gameplayTracks[5] = LoadMusicStream("res/music/level_music_06.mp3");
			menuMusic = LoadMusicStream("res/music/menu_music.mp3");
			buttonSfx=LoadSound("res/sfx/button_sfx.mp3");
			attackSfx = LoadSound("res/sfx/attack_sfx.mp3");
			weakAttackSfx = LoadSound("res/sfx/weak_attack_sfx.mp3");
			poweredAttackSfx = LoadSound("res/sfx/powered_attack_sfx.mp3");
			matchSfx = LoadSound("res/sfx/match_sfx.mp3");
			enemyHitSfx = LoadSound("res/sfx/enemy_hit_sfx.mp3");
			musicVolume[0] = 0.3f;
			musicVolume[1] = 0.2f;
			musicVolume[2] = 0.1f;
			musicVolume[3] = 0.14f;
			musicVolume[4] = 0.2f;
			musicVolume[5] = 0.08f;
			for (int i = 0; i < screens::amountOfWorlds; i++) {
				SetMusicVolume(gameplayTracks[i], musicVolume[i]);
			}
			SetMusicVolume(menuMusic, menuMusicVolume);
			SetSoundVolume(buttonSfx, buttonVolume);
			SetSoundVolume(enemyHitSfx, enemyHitVolume);
			SetSoundVolume(weakAttackSfx, weakAttackVolume);
			SetSoundVolume(attackSfx, attackVolume);
			SetSoundVolume(poweredAttackSfx, poweredAttackVolume);
			SetSoundVolume(matchSfx, matchVolume);
		}
		void mute() {
			for (int i = 0; i < screens::amountOfWorlds; i++) {
				SetMusicVolume(gameplayTracks[i], mutedVolume);
			}
			SetMusicVolume(menuMusic, mutedVolume);
			SetSoundVolume(buttonSfx, mutedVolume);
			SetSoundVolume(enemyHitSfx, mutedVolume);
			SetSoundVolume(weakAttackSfx, mutedVolume);
			SetSoundVolume(attackSfx, mutedVolume);
			SetSoundVolume(poweredAttackSfx, mutedVolume);
			SetSoundVolume(matchSfx, mutedVolume);
		}
		void unmute() {
			for (int i = 0; i < screens::amountOfWorlds; i++) {
				SetMusicVolume(gameplayTracks[i], musicVolume[i]);
			}
			SetMusicVolume(menuMusic, menuMusicVolume);
			SetSoundVolume(buttonSfx, buttonVolume);
			SetSoundVolume(enemyHitSfx, enemyHitVolume);
			SetSoundVolume(weakAttackSfx, weakAttackVolume);
			SetSoundVolume(attackSfx, attackVolume);
			SetSoundVolume(poweredAttackSfx, poweredAttackVolume);
			SetSoundVolume(matchSfx, matchVolume);
		}
		void deinit() {
			for (int i = 0; i < screens::amountOfWorlds; i++) {
				UnloadMusicStream(gameplayTracks[i]);
			}
			UnloadMusicStream(menuMusic);
			UnloadSound(buttonSfx);
			UnloadSound(attackSfx);
			UnloadSound(weakAttackSfx);
			UnloadSound(poweredAttackSfx);
			UnloadSound(matchSfx);
			UnloadSound(enemyHitSfx);
		}
	}
}