#ifndef AUDIO_H
#define AUDIO_H
#include "raylib.h"
#include "screens_manager/screens.h"

namespace match3 {
	namespace audio {

		extern Music gameplayTracks[screens::amountOfWorlds];
		extern Music menuMusic;
		extern Sound buttonSfx;
		extern Sound attackSfx;
		extern Sound weakAttackSfx;
		extern Sound poweredAttackSfx;
		extern Sound matchSfx;
		extern Sound enemyHitSfx;
		extern bool muted;

		void init();
		void mute();
		void unmute();
		void deinit();
	}
}
#endif

