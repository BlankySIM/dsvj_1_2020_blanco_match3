#ifndef RESULTS_H
#define RESULTS_H
#include "raylib.h"
#include "button/button.h"
#include "player/player.h"
#include "enemy/enemy.h"
#include "audio/audio.h"

namespace match3 {
	namespace results {

		void init();
		void inputs();
		void update();
		void draw();
		void resize();
		void deinit();
	}
}
#endif