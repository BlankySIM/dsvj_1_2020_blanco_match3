#include "results.h"

namespace match3 {
	namespace results {

		Vector2 mousePosition;
		button::BUTTON keepPlayingButton;
		button::BUTTON menuButton;

		void init() {
			keepPlayingButton.rec.width = button::normalButtonW;
			keepPlayingButton.rec.height = button::normalButtonH;
			menuButton.rec.width = button::normalButtonW;
			menuButton.rec.height = button::normalButtonH;
			keepPlayingButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			keepPlayingButton.rec.y = screens::middleScreenH - (screens::screenHeight / 10.0f);
			menuButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			menuButton.rec.y = keepPlayingButton.rec.y + keepPlayingButton.rec.height + (screens::middleScreenH / 5);
		}
		void inputs() {
			mousePosition = GetMousePosition();
		}
		void update() {
			PlayMusicStream(audio::gameplayTracks[screens::currentWorld]);
			UpdateMusicStream(audio::gameplayTracks[screens::currentWorld]);
			button::update(keepPlayingButton, mousePosition);
			button::update(menuButton, mousePosition);
			if (keepPlayingButton.active) {
				keepPlayingButton.active = !keepPlayingButton.active;
				screens::currentScreen = screens::gameplay_screen;
				if (screens::Iterator == screens::worldLevels-1) {
					StopMusicStream(audio::gameplayTracks[screens::currentWorld]);
				}
				screens::iterateLevel();
				player::matchPlayer.currentLevel++;
				if (!player::matchPlayer.alive || player::matchPlayer.currentLevel == screens::worldLevels * screens::amountOfWorlds + 1) {
					player::matchPlayer.currentLevel = 1;
					player::matchPlayer.enemiesDefeated = 0;
					player::matchPlayer.score = 0;
					screens::reinitIterator();
				}
				player::reinit();
				enemy::reinitEnemy();
				tile::reinitGrid();
			}
			else if (menuButton.active) {
				StopMusicStream(audio::gameplayTracks[screens::currentWorld]);
				menuButton.active = !menuButton.active;
				screens::currentScreen = screens::menu_screen;
				player::reinit();
				player::matchPlayer.currentLevel = 1;
				player::matchPlayer.enemiesDefeated = 0;
				player::matchPlayer.score = 0;
				screens::reinitIterator();
				enemy::reinitEnemy();
				tile::reinitGrid();
			}
		}
		void draw() {
			DrawTexture(sprites::backgrounds[screens::currentWorld], screens::screenZero, screens::screenZero, WHITE);
			DrawRectangle(screens::screenZero, screens::screenZero, screens::screenWidth,
				screens::screenHeight, Fade(BLACK, 0.6f));
			button::draw(keepPlayingButton, mousePosition);
			button::draw(menuButton, mousePosition);
			if (player::matchPlayer.alive) {
				DrawText("Victory", static_cast<int>(screens::middleScreenW - (screens::screenWidth / 16.0f)),
					screens::screenZero + (screens::middleScreenH / 20), screens::maxFontSize, WHITE);
				if (player::matchPlayer.currentLevel == screens::worldLevels * screens::amountOfWorlds) {
					DrawText("Congrats!", static_cast<int>(screens::middleScreenW - (screens::screenWidth / 18.0f)),
						screens::screenZero + (screens::middleScreenH / 2), screens::medFontSize, WHITE);
					DrawText("You finished all the levels",
						static_cast<int>(screens::middleScreenW - (screens::screenWidth / 6.5f)),
						screens::screenZero + static_cast<int>(screens::middleScreenH / 1.5f), screens::medFontSize, WHITE);
					DrawText("Restart", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 22,
						static_cast<int>(keepPlayingButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				}
				else {
					DrawText("Continue", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 20,
						static_cast<int>(keepPlayingButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				}
			}
			else {
				DrawText("Defeat", static_cast<int>(screens::middleScreenW - (screens::screenWidth / 19.0f)),
					screens::screenZero + (screens::middleScreenH / 20), screens::maxFontSize, WHITE);
				DrawText("Restart", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 22,
					static_cast<int>(keepPlayingButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
			}
			DrawText("Menu", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 34,
				static_cast<int>(menuButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
			DrawText(FormatText("Enemies defeated: %0i", player::matchPlayer.enemiesDefeated),
				static_cast<int>(screens::screenZero) + screens::screenWidth / 100,
				static_cast<int>(screens::screenZero + screens::screenHeight / 100), screens::medFontSize, WHITE);
			DrawText(FormatText("Score: %0i", player::matchPlayer.score),
				static_cast<int>(screens::screenZero + (screens::screenWidth / 100)),
				static_cast<int>(screens::screenZero + screens::screenHeight / 12.5f), screens::medFontSize, WHITE);
			DrawText(FormatText("HighestScore: %0i", player::matchPlayer.highestScore),
				static_cast<int>(screens::screenZero + (screens::screenWidth / 100)),
				static_cast<int>(screens::screenZero + screens::screenHeight / 6.5f), screens::medFontSize, YELLOW);
		}
		void resize() {
			keepPlayingButton.rec.width = button::normalButtonW;
			keepPlayingButton.rec.height = button::normalButtonH;
			menuButton.rec.width = button::normalButtonW;
			menuButton.rec.height = button::normalButtonH;
			keepPlayingButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			keepPlayingButton.rec.y = screens::middleScreenH - (screens::screenHeight / 10.0f);
			menuButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			menuButton.rec.y = keepPlayingButton.rec.y + keepPlayingButton.rec.height + (screens::middleScreenH / 5);
		}
		void deinit() {

		}
	}
}
