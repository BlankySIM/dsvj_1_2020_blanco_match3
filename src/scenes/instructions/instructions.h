#ifndef INSTRUCTIONS_H
#define	INSTRUCTIONS_H
#include "raylib.h"
#include "button/button.h"
#include "screens_manager/screens.h"
#include "audio/audio.h"
#include "sprites/sprites.h"

namespace match3 {
	namespace intructions {

		void init();
		void inputs();
		void update();
		void draw();
		void resize();
		void deinit();
	}
}
#endif
