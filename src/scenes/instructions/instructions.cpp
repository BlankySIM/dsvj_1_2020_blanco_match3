#include "instructions.h"

namespace match3 {
	namespace intructions {

		button::BUTTON backButton;
		Vector2 mousePosition;
		float maxTestBar;
		Rectangle testBar;
		float testBarIncreaser;

		static void moveTestBar() {
			testBar.width += testBarIncreaser * GetFrameTime();
			if (testBar.width>=maxTestBar) {
				testBar.width = 0;
			}
		}
		void init() {
			backButton.rec.height = button::normalButtonH;
			backButton.rec.width = button::normalButtonW;
			backButton.rec.x = screens::screenZero + screens::screenWidth / 80.0f;
			backButton.rec.y = (screens::screenHeight - backButton.rec.height) - screens::middleScreenH / 45.0f;
			maxTestBar = static_cast<float>(screens::screenWidth / 6.5f);
			testBar.width = 0;
			testBar.height = static_cast<float>(screens::screenHeight / 20);
			testBar.x = static_cast<float>(screens::screenZero + screens::screenWidth / 5);
			testBar.y = static_cast<float>(screens::screenZero + screens::screenHeight / 8);
			testBarIncreaser = static_cast<float>(screens::screenWidth / 20);
		}
		void inputs() {
			mousePosition = GetMousePosition();
		}
		void update() {
			moveTestBar();
			PlayMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::menuMusic);
			button::update(backButton, mousePosition);
			if (backButton.active) {
				backButton.active = !backButton.active;
				screens::currentScreen = screens::menu_screen;
			}
		}
		void draw() {
			sprites::drawStaticBackground();
			DrawRectangle(screens::screenZero, screens::screenZero, screens::screenWidth, screens::screenHeight, Fade(BLACK, 0.6f));
			DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero),
				static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenZero + screens::screenHeight / 10),
				BLACK);
			DrawText("Instructions", static_cast<int>(screens::middleScreenW - (screens::screenWidth / 9.9f)),
				screens::screenZero + (screens::middleScreenH / 20), screens::maxFontSize, WHITE);
			button::draw(backButton, mousePosition);
			DrawText("Back", static_cast<int>(backButton.rec.x) + screens::screenWidth / 21,
				static_cast<int>(backButton.rec.y) + screens::screenHeight / 32, screens::medFontSize, BLACK);
			for (int i = 0; i < 3; i++) {
				DrawTexture(sprites::tileSprite3, (screens::screenZero + screens::screenWidth / 200)+
					((sprites::tileSprite1.width+screens::screenWidth/200) * i),
					screens::screenZero + screens::screenHeight / 9, WHITE);
			}
			DrawRectangle(static_cast<int>(testBar.x), static_cast<int>(testBar.y),
				static_cast<int>(maxTestBar), static_cast<int>(testBar.height), WHITE);
			DrawRectangle(static_cast<int>(testBar.x), static_cast<int>(testBar.y),
				static_cast<int>(testBar.width), static_cast<int>(testBar.height), BLUE);
			DrawText("- Match 3 or more tiles of the same color to charge your attack bar.",
				screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 5, screens::medFontSize, WHITE);
			DrawText("- Once the bar is full you'l deal damage to the enemy",
				screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + screens::screenHeight / 4, screens::medFontSize, WHITE);
			DrawTexture(sprites::tileSprite5, (screens::screenZero + screens::screenWidth / 200),
				screens::screenZero + static_cast<int>(screens::screenHeight / 3.3f), WHITE);
			DrawText("->Weak", screens::screenZero + screens::screenWidth / 200 + (sprites::tileSprite1.width),
				screens::screenZero + static_cast<int>(screens::screenHeight / 3.3f)
				+ (sprites::tileSprite1.width/2) - (screens::medFontSize/2), screens::medFontSize, WHITE);
			DrawText("- Use the enemy's elemental weaknes to your advantage",
				screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + static_cast<int>(screens::screenHeight / 2.6f), screens::medFontSize, WHITE);
			DrawTexture(sprites::tileSprite2, (screens::screenZero + screens::screenWidth / 200),
				screens::screenZero + static_cast<int>(screens::screenHeight / 2.25f), WHITE);
			DrawText("->Resist", screens::screenZero + screens::screenWidth / 200 + (sprites::tileSprite1.width),
				screens::screenZero + static_cast<int>(screens::screenHeight / 2.25f)
				+ (sprites::tileSprite1.width / 2) - (screens::medFontSize / 2), screens::medFontSize, WHITE);
			DrawText("- And try to avoid the enemy's resistances",
				screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + static_cast<int>(screens::screenHeight / 1.9f), screens::medFontSize, WHITE);

			DrawTexture(sprites::tileSprite1, (screens::screenZero + screens::screenWidth / 200),
				screens::screenZero + static_cast<int>(screens::screenHeight / 1.72f), WHITE);
			DrawText("->Next atq", screens::screenZero + screens::screenWidth / 200 + (sprites::tileSprite1.width),
				screens::screenZero + static_cast<int>(screens::screenHeight / 1.72f)
				+ (sprites::tileSprite1.width / 2) - (screens::medFontSize / 2), screens::medFontSize, WHITE);
			DrawText("- The element with the most matches will determine the element of your next attack",
				screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + static_cast<int>(screens::screenHeight / 1.5f), screens::medFontSize, WHITE);
			DrawText("Defeat the enemy before he defeats you!",
				screens::screenZero + screens::screenWidth / 200,
				screens::screenZero + static_cast<int>(screens::screenHeight / 1.3f), screens::medFontSize, YELLOW);
		}
		void resize() {
			backButton.rec.height = button::normalButtonH;
			backButton.rec.width = button::normalButtonW;
			backButton.rec.x = screens::screenZero + screens::screenWidth / 80.0f;
			backButton.rec.y = (screens::screenHeight - backButton.rec.height) - screens::middleScreenH / 45.0f;
			maxTestBar = static_cast<float>(screens::screenWidth / 6.5f);
			testBar.height = static_cast<float>(screens::screenHeight / 20);
			testBar.x = static_cast<float>(screens::screenZero + screens::screenWidth / 5);
			testBar.y = static_cast<float>(screens::screenZero + screens::screenHeight / 8);
			testBarIncreaser = static_cast<float>(screens::screenWidth / 20);
		}
		void deinit() {

		}
	}
}