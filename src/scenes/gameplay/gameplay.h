#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include "tile/tile.h"
#include "player/player.h"
#include "enemy/enemy.h"
#include "sprites/sprites.h"
#include "audio/audio.h"
#include "button/button.h"

namespace match3 {
	namespace gameplay {

		void init();
		void inputs();
		void update();
		void draw();
		void resize();
		void deinit();
	}
}
#endif