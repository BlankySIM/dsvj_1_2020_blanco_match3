#include "gameplay.h"

namespace match3 {
	namespace gameplay {

		button::BUTTON unPauseButton;
		button::BUTTON menuButton;
		button::BUTTON pauseButton;
		button::BUTTON muteButton;
		bool started = false;
		const float startMaxCounter = 100.0f;
		float startCounter = startMaxCounter;
		const float counterDecreaser = 30.0f;
		Vector2 mousePosition;

		static void drawStart() {
			if (!started) {
				DrawRectangle(screens::screenZero, screens::screenZero,
					screens::screenWidth,screens::screenHeight, Fade(BLACK, 0.9f));
				if (startCounter >= startMaxCounter * 0.75f) {
					DrawText("3", screens::middleScreenW - (screens::screenWidth / 100),
						screens::middleScreenH - (screens::maxFontSize / 2),screens::maxFontSize, WHITE);
				}
				else if (startCounter >= startMaxCounter * 0.5f) {
					DrawText("2", screens::middleScreenW - (screens::screenWidth / 100),
						screens::middleScreenH - (screens::maxFontSize / 2), screens::maxFontSize, WHITE);
				}
				else if (startCounter >= startMaxCounter * 0.25f) {
					DrawText("1", screens::middleScreenW - (screens::screenWidth / 150),
						screens::middleScreenH - (screens::maxFontSize / 2), screens::maxFontSize, WHITE);
				}
				else {
					DrawText("START", screens::middleScreenW - (screens::screenWidth / 18),
						screens::middleScreenH - (screens::maxFontSize / 2), screens::maxFontSize, WHITE);
				}
			}
		}
		static void startAnimationUpdate() {
			if (!started) {
				startCounter -= counterDecreaser * GetFrameTime();
				if (startCounter <= 0) {
					startCounter = startMaxCounter;
					started=true;
				}
			}
		}
		void init() {
			tile::init();
			player::init();
			enemy::init();		
			pauseButton.rec.height = button::tinyButtonH;
			pauseButton.rec.width = button::tinyButtonW;
			pauseButton.rec.x = screens::middleScreenW - pauseButton.rec.width / 2;
			pauseButton.rec.y = screens::screenZero + static_cast<float>(screens::screenHeight / 200);
			unPauseButton.rec.width = button::normalButtonW;
			unPauseButton.rec.height = button::normalButtonH;
			menuButton.rec.width = button::normalButtonW;
			menuButton.rec.height = button::normalButtonH;
			unPauseButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			unPauseButton.rec.y = screens::middleScreenH - (screens::screenHeight / 10.0f);
			menuButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			menuButton.rec.y = unPauseButton.rec.y + unPauseButton.rec.height + (screens::middleScreenH / 5);
			muteButton.rec.width = button::tinyButtonW;
			muteButton.rec.height = button::tinyButtonH;
			muteButton.rec.x = screens::screenZero + (screens::screenWidth / 100.0f);
			muteButton.rec.y = screens::screenZero + (screens::screenWidth / 100.0f);
		}
		void inputs() {
			player::inputs();
			mousePosition = GetMousePosition();
		}
		void update() {
			PlayMusicStream(audio::gameplayTracks[screens::currentWorld]);
			UpdateMusicStream(audio::gameplayTracks[screens::currentWorld]);
			if (started) {

				if (!pauseButton.active) {
					button::update(pauseButton, mousePosition);
					player::update(enemy::enemy.lifeBar, enemy::enemy.weaknessType, enemy::enemy.resistanceType);
					enemy::update(player::matchPlayer.lifeBar);
					if (!player::matchPlayer.alive || !enemy::enemy.alive) {
						if (!enemy::enemy.alive) {
							player::matchPlayer.enemiesDefeated++;
						}
						screens::currentScreen = screens::results_screen;
						started = false;
					}
					#if DEBUG
					if (IsKeyPressed(KEY_A)) {
						if (!enemy::enemy.alive) {
							player::matchPlayer.enemiesDefeated++;
						}
						screens::currentScreen = screens::results_screen;
						started = false;
					}
					#endif
				}
				else {
					button::update(unPauseButton, mousePosition);
					button::update(menuButton, mousePosition);
					button::updateMuteButton(muteButton, mousePosition);
					if (unPauseButton.active) {
						unPauseButton.active = !unPauseButton.active;
						pauseButton.active = false;
					}
					else if (menuButton.active) {
						menuButton.active = !menuButton.active;
						pauseButton.active = false;
						StopMusicStream(audio::gameplayTracks[screens::currentWorld]);
						screens::currentScreen = screens::menu_screen;
						player::reinit();
						player::matchPlayer.currentLevel = 1;
						player::matchPlayer.enemiesDefeated = 0;
						player::matchPlayer.score = 0;
						screens::reinitIterator();
						enemy::reinitEnemy();
						tile::reinitGrid();
						started = false;
					}
				}
			}
			else {
				startAnimationUpdate();
			}
		}
		void draw() {
			DrawTexture(sprites::backgrounds[screens::currentWorld], screens::screenZero, screens::screenZero, WHITE);
			DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero),
				static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenZero + screens::screenHeight / 15),
				BLACK);
			tile::draw(player::matchPlayer.selecting);
			#if DEBUG
			DrawText("DEBUG || (A) button -> Next level", screens::screenWidth - static_cast<int>(screens::screenWidth / 2.5f),
				screens::screenZero + screens::screenWidth / 100, screens::medFontSize, WHITE);
			#endif
			drawStart();
			player::draw(enemy::enemy.attacking);
			enemy::draw(player::matchPlayer.attacking);
			DrawText(FormatText("Level %0i", player::matchPlayer.currentLevel),
				static_cast<int>(screens::screenZero + (screens::screenWidth / 100)),
				static_cast<int>(screens::screenZero + screens::screenHeight / 100), screens::medFontSize, WHITE);
			if (!pauseButton.active) {
				if (started) {
					button::drawPauseButton(pauseButton, mousePosition);
				}
			}
			else{
				DrawRectangle(screens::screenZero, screens::screenZero,
					screens::screenWidth, screens::screenHeight, Fade(BLACK, 0.9f));
				button::draw(unPauseButton, mousePosition);
				button::draw(menuButton, mousePosition);
				button::drawMuteButton(muteButton, mousePosition);
				DrawText("Continue", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 20,
					static_cast<int>(unPauseButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("Menu", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 34,
					static_cast<int>(menuButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
			}
		}
		void resize() {
			pauseButton.rec.height = button::tinyButtonH;
			pauseButton.rec.width = button::tinyButtonW;
			pauseButton.rec.x = screens::middleScreenW - pauseButton.rec.width / 2;
			pauseButton.rec.y = screens::screenZero + static_cast<float>(screens::screenHeight / 200);
			unPauseButton.rec.width = button::normalButtonW;
			unPauseButton.rec.height = button::normalButtonH;
			menuButton.rec.width = button::normalButtonW;
			menuButton.rec.height = button::normalButtonH;
			unPauseButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			unPauseButton.rec.y = screens::middleScreenH - (screens::screenHeight / 10.0f);
			menuButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			menuButton.rec.y = unPauseButton.rec.y + unPauseButton.rec.height + (screens::middleScreenH / 5);
			muteButton.rec.width = button::tinyButtonW;
			muteButton.rec.height = button::tinyButtonH;
			muteButton.rec.x = screens::screenZero + (screens::screenWidth / 100.0f);
			muteButton.rec.y = screens::screenZero + (screens::screenWidth / 100.0f);
		}
		void deinit() {
			
		}
	}
}