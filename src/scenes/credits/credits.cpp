#include "credits.h"

namespace match3 {
	namespace credits {

		enum Pages{
			page1,
			page2,
		};
		button::BUTTON backButton;
		button::BUTTON nextPageButton;
		button::BUTTON previousPageButton;
		Vector2 mousePosition;
		Pages currentPage = page1;

		void init() {
			backButton.rec.height = button::normalButtonH;
			backButton.rec.width = button::normalButtonW;
			backButton.rec.x = screens::screenZero + screens::screenWidth / 80.0f;
			backButton.rec.y = (screens::screenHeight - backButton.rec.height) - screens::middleScreenH / 45.0f;
			nextPageButton.rec.height = button::tinyButtonH;
			nextPageButton.rec.width = button::tinyButtonW;
			nextPageButton.rec.x = screens::screenWidth - button::tinyButtonW - screens::screenWidth / 100.0f;
			nextPageButton.rec.y = screens::screenZero + screens::screenWidth / 100.0f;
			previousPageButton.rec.height = button::tinyButtonH;
			previousPageButton.rec.width = button::tinyButtonW;
			previousPageButton.rec.x = screens::screenZero + screens::screenWidth / 100.0f;
			previousPageButton.rec.y = screens::screenZero + screens::screenWidth / 100.0f;
		}
		void inputs() {
			mousePosition = GetMousePosition();
		}
		void update() {
			PlayMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::menuMusic);
			button::update(backButton, mousePosition);
			switch (currentPage){
			case match3::credits::page1:
				button::update(nextPageButton, mousePosition);
				break;
			case match3::credits::page2:
				button::update(previousPageButton, mousePosition);
				break;
			}
			if (backButton.active) {
				backButton.active = !backButton.active;
				screens::currentScreen = screens::menu_screen;
				currentPage = page1;
			}
			else if (nextPageButton.active) {
				nextPageButton.active = !nextPageButton.active;
				currentPage = page2;
			}
			else if (previousPageButton.active) {
				previousPageButton.active = !previousPageButton.active;
				currentPage = page1;
			}
		}
		void draw() {
			sprites::drawStaticBackground();
			DrawRectangle(screens::screenZero, screens::screenZero, screens::screenWidth, screens::screenHeight, Fade(BLACK, 0.7f));
			DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero),
				static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenZero + screens::screenHeight / 10),
				BLACK);
			DrawText("Credits", screens::middleScreenW - (screens::screenWidth / 17),
				screens::screenZero + (screens::middleScreenH / 20), screens::maxFontSize, WHITE);
			button::draw(backButton, mousePosition);
			DrawText("Back", static_cast<int>(backButton.rec.x) + screens::screenWidth / 21,
				static_cast<int>(backButton.rec.y) + screens::screenHeight / 32, screens::medFontSize, BLACK);
			switch (currentPage) {
			case match3::credits::page1:
				button::drawNextButton(nextPageButton, mousePosition);
				DrawText("GameDev:", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10, screens::medFontSize, WHITE);
				DrawText("- Blanco Juan Simon: blancojuansimon@gmail.com || https://blankysim.itch.io", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + screens::maxFontSize, screens::minFontsize, SKYBLUE);
				DrawText("Game Art:", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + screens::screenHeight/10, screens::medFontSize, WHITE);
				DrawText("- Enemies by: http://bevouliin.com", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + screens::screenHeight / 10 + screens::maxFontSize, screens::minFontsize, SKYBLUE);
				DrawText("Licences: * https://creativecommons.org/licenses/by/4.0/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + screens::screenHeight / 5, screens::minFontsize, WHITE);
				DrawText("* https://creativecommons.org/licenses/by/3.0/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + screens::screenHeight / 5 + screens::medFontSize, screens::minFontsize, WHITE);
				DrawText("- Backgrounds by: https://madfireongames.com", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 3.5f), screens::minFontsize, SKYBLUE);
				DrawText("- Tiles by: https://opengameart.org/users/chiligames", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 3.0f), screens::minFontsize, SKYBLUE);
				DrawText("- UI by: https://www.gameart2d.com", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 2.6f), screens::minFontsize, SKYBLUE);
				DrawText("- Golden knight by: https://opengameart.org/users/dontmind8", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 2.3f), screens::minFontsize, SKYBLUE);
				DrawText("Licences: * https://creativecommons.org/licenses/by/4.0/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 2.08f), screens::minFontsize, WHITE);
				DrawText("* https://creativecommons.org/licenses/by/3.0/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 1.9f), screens::minFontsize, WHITE);
				break;
			case match3::credits::page2:
				button::drawPreviousButton(previousPageButton, mousePosition);
				DrawText("Game Audio:", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10, screens::medFontSize, WHITE);
				DrawText("- 'Winter Wind' by: https://opengameart.org/users/wipics", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + screens::maxFontSize, screens::minFontsize, SKYBLUE);
				DrawText("- 'Night Escape' by: https://opengameart.org/users/agecaf", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + static_cast<int>(screens::screenHeight / 9.6f), screens::minFontsize, SKYBLUE);
				DrawText("- 'Hella Bumps' by: https://opengameart.org/users/cynicmusic", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + screens::screenHeight / 10 + screens::maxFontSize, screens::minFontsize, SKYBLUE);
				DrawText("- 'Menu Music' by: https://opengameart.org/users/mrpoly", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + screens::screenHeight / 5, screens::minFontsize, SKYBLUE);
				DrawText("- 'Beyond The Clouds (Orchestral Theme)' by: https://opengameart.org/users/matthew-pablo", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 + screens::screenHeight / 5 + screens::medFontSize, screens::minFontsize, SKYBLUE);
				DrawText("License: * https://creativecommons.org/licenses/by/3.0/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 3.5f), screens::minFontsize, WHITE);
				DrawText("- 'Game Music Loop - Intense' by: https://opengameart.org/users/horrorpen", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 3.0f), screens::minFontsize, SKYBLUE);
				DrawText("License: * https://creativecommons.org/licenses/by/3.0/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 2.6f), screens::minFontsize, WHITE);
				DrawText("- 'Night In The Desert' by: https://opengameart.org/users/tausdei", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 2.3f), screens::minFontsize, SKYBLUE);
				DrawText("Licences: * https://creativecommons.org/licenses/by/4.0/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 2.08f), screens::minFontsize, WHITE);
				DrawText("* https://creativecommons.org/licenses/by/3.0/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 1.9f), screens::minFontsize, WHITE);

				DrawText("SFX by: -freesfx.co.uk", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 1.78f), screens::minFontsize, SKYBLUE);
				DrawText("-https://freesound.org/people/Screamr/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 1.68f), screens::minFontsize, SKYBLUE);
				DrawText("-https://freesound.org/people/suntemple/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 1.58f), screens::minFontsize, SKYBLUE);
				DrawText("-https://freesound.org/people/NeoSpica/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 1.48f), screens::minFontsize, SKYBLUE);
				DrawText("-https://freesound.org/people/rcroller/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 1.4f), screens::minFontsize, SKYBLUE);
				DrawText("-https://freesound.org/people/plasterbrain/", screens::screenZero + screens::screenWidth / 200,
					screens::screenZero + screens::screenHeight / 10 +
					static_cast<int>(screens::screenHeight / 1.33f), screens::minFontsize, SKYBLUE);
				break;
			}
		}
		void resize() {
			backButton.rec.height = button::normalButtonH;
			backButton.rec.width = button::normalButtonW;
			backButton.rec.x = screens::screenZero + screens::screenWidth / 80.0f;
			backButton.rec.y = (screens::screenHeight - backButton.rec.height) - screens::middleScreenH / 45.0f;
			nextPageButton.rec.height = button::tinyButtonH;
			nextPageButton.rec.width = button::tinyButtonW;
			nextPageButton.rec.x = screens::screenWidth - button::tinyButtonW - screens::screenWidth / 100.0f;
			nextPageButton.rec.y = screens::screenZero + screens::screenWidth / 100.0f;
			previousPageButton.rec.height = button::tinyButtonH;
			previousPageButton.rec.width = button::tinyButtonW;
			previousPageButton.rec.x = screens::screenZero + screens::screenWidth / 100.0f;
			previousPageButton.rec.y = screens::screenZero + screens::screenWidth / 100.0f;
		}
		void deinit() {
		
		}
	}
}