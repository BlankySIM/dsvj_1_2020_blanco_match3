#ifndef CREDITS_H
#define CREDITS_H
#include "raylib.h"
#include "button/button.h"
#include "screens_manager/screens.h"
#include "audio/audio.h"

namespace match3 {
	namespace credits {

		void init();
		void inputs();
		void update();
		void draw();
		void resize();
		void deinit();
	}
}
#endif
