#ifndef	MENU_H
#define	MENU_H
#include "raylib.h"
#include "button/button.h"
#include "screens_manager/screens.h"
#include "sprites/sprites.h"
#include "audio/audio.h"
#include "player/player.h"

namespace match3 {
	namespace menu {
		
		void init();
		void inputs();
		void update();
		void draw();
		void resize();
		void deinit();
	}
}
#endif