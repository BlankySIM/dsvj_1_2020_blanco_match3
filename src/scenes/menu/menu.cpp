#include "menu.h"

namespace match3 {
	namespace menu {

		button::BUTTON startButton;
		button::BUTTON creditsButton;
		button::BUTTON instructionsButton;
		button::BUTTON muteButton;
		button::BUTTON settingsButton;
		button::BUTTON backButton;
		button::BUTTON bigResButton;
		button::BUTTON mediumResButton;
		button::BUTTON smallResButton;
		Vector2 mousePosition;

		void init() {
			audio::init();
			sprites::init();
			button::initButtonSizes();
			startButton.rec.width = button::normalButtonW;
			startButton.rec.height = button::normalButtonH;
			creditsButton.rec.width = button::normalButtonW;
			creditsButton.rec.height = button::normalButtonH;
			instructionsButton.rec.width = button::normalButtonW;
			instructionsButton.rec.height = button::normalButtonH;
			muteButton.rec.width = button::tinyButtonW;
			muteButton.rec.height = button::tinyButtonH;
			settingsButton.rec.width = button::tinyButtonW;
			settingsButton.rec.height = button::tinyButtonH;
			startButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			startButton.rec.y = screens::middleScreenH - (screens::screenHeight / 10.0f);
			creditsButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			creditsButton.rec.y = startButton.rec.y + startButton.rec.height + (screens::middleScreenH / 5);
			instructionsButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2);
			instructionsButton.rec.y = creditsButton.rec.y + creditsButton.rec.height + (screens::middleScreenH / 5);
			muteButton.rec.x = screens::screenZero + (screens::screenWidth / 100.0f);
			muteButton.rec.y = screens::screenZero + (screens::screenWidth / 100.0f);
			settingsButton.rec.x = screens::screenZero + (screens::screenWidth / 100.0f) +
				settingsButton.rec.width + (screens::screenWidth/100);
			settingsButton.rec.y = screens::screenZero + (screens::screenWidth / 100.0f);
			backButton.rec.height = button::normalButtonH;
			backButton.rec.width = button::normalButtonW;
			backButton.rec.x = screens::screenZero + screens::screenWidth / 80.0f;
			backButton.rec.y = (screens::screenHeight - backButton.rec.height) - screens::middleScreenH / 45.0f;
			bigResButton = startButton;
			mediumResButton = creditsButton;
			smallResButton = instructionsButton;
		}
		void inputs() {
			mousePosition = GetMousePosition();
		}
		void update() {
			PlayMusicStream(audio::menuMusic);
			UpdateMusicStream(audio::menuMusic);
			if (settingsButton.active) {
				button::update(bigResButton, mousePosition);
				button::update(mediumResButton, mousePosition);
				button::update(smallResButton, mousePosition);
				button::update(backButton, mousePosition);
				if (bigResButton.active && screens::screenWidth != screens::bigResW) {
					bigResButton.active = !bigResButton.active;
					screens::resizeWindow = true;
					screens::screenWidth = screens::bigResW;
					screens::screenHeight = screens::bigResH;
					SetWindowSize(screens::screenWidth, screens::screenHeight);
					screens::resize();
				}
				else if (mediumResButton.active && screens::screenWidth != screens::medResW) {
					mediumResButton.active = !mediumResButton.active;
					screens::resizeWindow = true;
					screens::screenWidth = screens::medResW;
					screens::screenHeight = screens::medResH;
					SetWindowSize(screens::screenWidth, screens::screenHeight);
					screens::resize();
				}
				else if (smallResButton.active && screens::screenWidth != screens::smallResW)	{
					smallResButton.active = !smallResButton.active;
					screens::resizeWindow = true;
					screens::screenWidth = screens::smallResW;
					screens::screenHeight = screens::smallResH;
					SetWindowSize(screens::screenWidth, screens::screenHeight);
					screens::resize();
				}
				else if (backButton.active) {
					backButton.active = !backButton.active;
					settingsButton.active = !settingsButton.active;
				}
			}
			else {
				button::update(startButton, mousePosition);
				button::update(creditsButton, mousePosition);
				button::update(instructionsButton, mousePosition);
				button::update(settingsButton, mousePosition);
				button::updateMuteButton(muteButton, mousePosition);
				if (startButton.active) {
					startButton.active = !startButton.active;
					screens::currentScreen = screens::gameplay_screen;
					StopMusicStream(audio::menuMusic);
				}
				else if (creditsButton.active) {
					creditsButton.active = !creditsButton.active;
					screens::currentScreen = screens::credits_screen;
				}
				else if (instructionsButton.active) {
					instructionsButton.active = !instructionsButton.active;
					screens::currentScreen = screens::instructions_screen;
				}
			}
		}
		void draw() {
			sprites::drawStaticBackground();
			DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero),
				static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenZero + screens::screenHeight / 10),
				BLACK);
			DrawRectangle(static_cast<int>(screens::screenZero), static_cast<int>(screens::screenZero + screens::screenHeight / 10),
				static_cast<int>(screens::screenWidth), static_cast<int>(screens::screenZero + screens::screenHeight / 15),
				Fade(BLACK, 0.5f));
			DrawText("Puzzle Match", static_cast<int>(screens::middleScreenW - (screens::screenWidth / 9.5f)),
				screens::screenZero + (screens::middleScreenH / 20), screens::maxFontSize, WHITE);
			if (!settingsButton.active) {
				button::draw(startButton, mousePosition);
				button::draw(creditsButton, mousePosition);
				button::draw(instructionsButton, mousePosition);
				button::drawMuteButton(muteButton, mousePosition);
				button::drawSettingsButton(settingsButton, mousePosition);
				DrawText("Start", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 30,
					static_cast<int>(startButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("Credits", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 23,
					static_cast<int>(creditsButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("Help", static_cast<int>(screens::middleScreenW - screens::screenWidth / 39.0f),
					static_cast<int>(instructionsButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("v1.0.0", static_cast<int>(screens::screenZero + screens::screenWidth / 100.0f),
					static_cast<int>(screens::screenHeight - (screens::medFontSize + screens::screenHeight / 100)), screens::medFontSize, WHITE);
				DrawText(FormatText("HighestScore: %0i", player::matchPlayer.highestScore),
					static_cast<int>(screens::screenZero + (screens::screenWidth / 100)),
					static_cast<int>(screens::screenZero + screens::screenHeight / 9.0f), screens::medFontSize, YELLOW);
			}
			else {
				button::draw(bigResButton, mousePosition);
				button::draw(mediumResButton, mousePosition);
				button::draw(smallResButton, mousePosition);
				button::draw(backButton, mousePosition);
				DrawText("1280x720", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 20,
					static_cast<int>(startButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("1152x648", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 20,
					static_cast<int>(creditsButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("1024x576", static_cast<int>(screens::middleScreenW) - screens::screenWidth / 20,
					static_cast<int>(instructionsButton.rec.y) + screens::screenHeight / 34, screens::medFontSize, BLACK);
				DrawText("Back", static_cast<int>(backButton.rec.x) + screens::screenWidth / 21,
					static_cast<int>(backButton.rec.y) + screens::screenHeight / 32, screens::medFontSize, BLACK);
			}
		}
		void resize() {
			startButton.rec.width = button::normalButtonW;
			startButton.rec.height = button::normalButtonH;
			creditsButton.rec.width = button::normalButtonW;
			creditsButton.rec.height = button::normalButtonH;
			instructionsButton.rec.width = button::normalButtonW;
			instructionsButton.rec.height = button::normalButtonH;
			muteButton.rec.width = button::tinyButtonW;
			muteButton.rec.height = button::tinyButtonH;
			settingsButton.rec.width = button::tinyButtonW;
			settingsButton.rec.height = button::tinyButtonH;
			startButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			startButton.rec.y = screens::middleScreenH - (screens::screenHeight / 10.0f);
			creditsButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2.0f);
			creditsButton.rec.y = startButton.rec.y + startButton.rec.height + (screens::middleScreenH / 5);
			instructionsButton.rec.x = screens::middleScreenW - (button::normalButtonW / 2);
			instructionsButton.rec.y = creditsButton.rec.y + creditsButton.rec.height + (screens::middleScreenH / 5);
			muteButton.rec.x = screens::screenZero + (screens::screenWidth / 100.0f);
			muteButton.rec.y = screens::screenZero + (screens::screenWidth / 100.0f);
			settingsButton.rec.x = screens::screenZero + (screens::screenWidth / 100.0f) +
				settingsButton.rec.width + (screens::screenWidth / 100);
			settingsButton.rec.y = screens::screenZero + (screens::screenWidth / 100.0f);
			backButton.rec.height = button::normalButtonH;
			backButton.rec.width = button::normalButtonW;
			backButton.rec.x = screens::screenZero + screens::screenWidth / 80.0f;
			backButton.rec.y = (screens::screenHeight - backButton.rec.height) - screens::middleScreenH / 45.0f;
			bigResButton = startButton;
			mediumResButton = creditsButton;
			smallResButton = instructionsButton;
		}
		void deinit() {
			audio::deinit();
			sprites::deinit();
		}
	}
}