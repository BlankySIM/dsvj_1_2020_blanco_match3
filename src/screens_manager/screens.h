#ifndef SCREENS_H
#define SCREENS_H

namespace match3 {
	namespace screens {

		const int bigResW = 1280;
		const int medResW = 1152;
		const int smallResW = 1024;
		const int bigResH = 720;
		const int medResH = 648;
		const int smallResH = 576;
		const int targetFPS = 200;
		const int amountOfWorlds = 6;
		const int worldLevels = 3;
		extern int screenHeight;
		extern int screenWidth;
		extern int middleScreenH;
		extern int middleScreenW;
		extern int screenZero;
		extern int minFontsize;
		extern int medFontSize;
		extern int maxFontSize;
		extern int currentWorld;
		extern int Iterator;
		extern bool resizeWindow;
		enum scenes {
			gameplay_screen,
			menu_screen,
			credits_screen,
			instructions_screen,
			results_screen,
		};

		void reinitIterator();
		void iterateLevel();
		void resize();
		extern scenes currentScreen;
	}
}
#endif
