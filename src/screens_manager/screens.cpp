#include "screens.h"

namespace match3 {
	namespace screens {		
		int screenHeight = 648;
		int screenWidth = 1152;
		int screenZero = 0;
		int middleScreenH = screenHeight / 2;
		int middleScreenW = screenWidth / 2;
		int minFontsize = screenHeight / 36;
		int medFontSize = screenHeight / 24;
		int maxFontSize = screenHeight / 18;
		int currentWorld = 0;
		int Iterator = 0;		
		bool resizeWindow = false;

		void reinitIterator() {
			currentWorld = 0;
			Iterator = 0;
		}
		void iterateLevel() {
			Iterator++;
			if (Iterator > worldLevels - 1) {
				currentWorld++;
				Iterator = 0;
			}
			if (currentWorld > amountOfWorlds - 1) {
				reinitIterator();
			}
		}
		void resize() {
			middleScreenH = screenHeight / 2;
			middleScreenW = screenWidth / 2;
			minFontsize = screenHeight / 36;
			medFontSize = screenHeight / 24;
			maxFontSize = screenHeight / 18;
		}
		scenes currentScreen = menu_screen;
	}
}