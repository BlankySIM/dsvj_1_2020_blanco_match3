#include "enemy.h"

namespace match3 { 
	namespace enemy {

		float maxAttackBar;
		float maxLifeBar;
		const int amountOfEnemies = 6;
		const int worldLevels = 4;
		int framesCounter = 0;
		int frameSpeed = 2;
		int currentFrame = 0;
		int idleFrames = 2;
		Rectangle frameRec;
		Vector2 enemySpritePos;
		const float counterDecreaser = 100.f;
		const float attackMaxCounter = 100.0f;
		const float maxInvisibleTime = 20.0f;
		bool invisible = false;
		float attackAnimationCounter = attackMaxCounter;
		float invisivilityCounter = maxInvisibleTime;
		enum Type {
			type1,
			type2,
			type3,
			type4,
			type5,
			type6,
		};

		static void invisibleFrames() {
			invisivilityCounter -= counterDecreaser * GetFrameTime();
			if (invisivilityCounter <= 0) {
				invisivilityCounter = maxInvisibleTime;
				invisible = !invisible;
			}
		}
		static void enemyAttackAnimation() {
			if (enemy.attacking) {
				attackAnimationCounter -= counterDecreaser * GetFrameTime();
				if (attackAnimationCounter <= 0) {
					attackAnimationCounter = attackMaxCounter;
					enemy.attacking = false;
					invisible = false;
				}
			}
		}
		static void updateEnemyrFrames()
		{
			framesCounter++;

			if (framesCounter >= (screens::targetFPS / frameSpeed))
			{
				framesCounter = 0;
				currentFrame++;
				if (currentFrame >= idleFrames) currentFrame = 0;

				frameRec.x = static_cast<float>(currentFrame * (enemy.sprite.width / idleFrames));
			}
		}
		static void generateElementEffectivenesses() {
			int random = GetRandomValue(type1, type5);
			int random2 = 0;
			switch (random)
			{
			case type1:
				enemy.weaknessType = type1;
				enemy.weakneSprite = sprites::tileSprite1;
				break;
			case type2:
				enemy.weaknessType = type2;
				enemy.weakneSprite = sprites::tileSprite2;
				break;
			case type3:
				enemy.weaknessType = type3;
				enemy.weakneSprite = sprites::tileSprite3;
				break;
			case type4:
				enemy.weaknessType = type4;
				enemy.weakneSprite = sprites::tileSprite4;
				break;
			case type5:
				enemy.weaknessType = type5;
				enemy.weakneSprite = sprites::tileSprite5;
				break;
			}
			do {
				random2 = GetRandomValue(type1, type5);
			} while (random2 == random);
			switch (random2)
			{
			case type1:
				enemy.resistanceType = type1;
				enemy.resistanceSprite = sprites::tileSprite1;
				break;
			case type2:
				enemy.resistanceType = type2;
				enemy.resistanceSprite = sprites::tileSprite2;
				break;
			case type3:
				enemy.resistanceType = type3;
				enemy.resistanceSprite = sprites::tileSprite3;
				break;
			case type4:
				enemy.resistanceType = type4;
				enemy.resistanceSprite = sprites::tileSprite4;
				break;
			case type5:
				enemy.resistanceType = type5;
				enemy.resistanceSprite = sprites::tileSprite5;
				break;
			}
		}
		static void setEnemyDeath() {
			if (enemy.lifeBar.width <= 0) {
				enemy.alive = false;
			}
		}
		void init() {
			enemy.alive = true;
			enemy.damage = static_cast<float>(screens::screenWidth / 110);
			enemy.attackSpeed = static_cast<float>(screens::screenWidth / 110);
			maxAttackBar = screens::screenWidth / 6.4f;
			maxLifeBar = maxAttackBar;
			enemy.attackBar.height = screens::screenHeight / 24.0f;
			enemy.attackBar.width = 0.0f;
			enemy.attackBar.x = screens::screenWidth - maxAttackBar - screens::screenWidth / 12;
			enemy.attackBar.y = screens::middleScreenH - enemy.attackBar.height - screens::screenHeight / 9;
			enemy.lifeBar.height = screens::screenHeight / 24.0f;
			enemy.lifeBar.width = maxLifeBar;
			enemy.lifeBar.x = screens::screenWidth - maxLifeBar - screens::screenWidth / 12;
			enemy.lifeBar.y = screens::middleScreenH + enemy.attackBar.height + screens::screenHeight / 4;
			generateElementEffectivenesses();
			enemy.sprite = sprites::wormEnemy;
			frameRec.width = static_cast<float>(enemy.sprite.width / idleFrames);
			frameRec.height = static_cast<float>(enemy.sprite.height);
			enemy.attacking = false;
			currentFrame = 0;
			frameRec.x = 0;
		}
		void reinitEnemy() {
			switch (screens::currentWorld)
			{
			case type1:
				enemy.alive = true;
				enemy.attackBar.width = 0.0f;
				enemy.lifeBar.width = maxLifeBar;
				enemy.damage = static_cast<float>(screens::screenWidth / 100);
				enemy.attackSpeed = static_cast<float>(screens::screenWidth / 100);
				enemy.sprite = sprites::wormEnemy;
				break;
			case type2:
				enemy.alive = true;
				enemy.attackBar.width = 0.0f;
				enemy.lifeBar.width = maxLifeBar;
				enemy.damage = static_cast<float>(screens::screenWidth / 90);
				enemy.attackSpeed = static_cast<float>(screens::screenWidth / 90);
				enemy.sprite = sprites::zombieEnemy;
				break;
			case type3:
				enemy.alive = true;
				enemy.attackBar.width = 0.0f;
				enemy.lifeBar.width = maxLifeBar;
				enemy.damage = static_cast<float>(screens::screenWidth / 80);
				enemy.attackSpeed = static_cast<float>(screens::screenWidth / 80);
				enemy.sprite = sprites::skullEnemy;
				break;
			case type4:
				enemy.alive = true;
				enemy.attackBar.width = 0.0f;
				enemy.lifeBar.width = maxLifeBar;
				enemy.damage = static_cast<float>(screens::screenWidth / 70);
				enemy.attackSpeed = static_cast<float>(screens::screenWidth / 70);
				enemy.sprite = sprites::plantEnemy;
				break;
			case type5:
				enemy.alive = true;
				enemy.attackBar.width = 0.0f;
				enemy.lifeBar.width = maxLifeBar;
				enemy.damage = static_cast<float>(screens::screenWidth / 75);
				enemy.attackSpeed = static_cast<float>(screens::screenWidth / 75);
				enemy.sprite = sprites::darkEnemy;
				break;
			case type6:
				enemy.alive = true;
				enemy.attackBar.width = 0.0f;
				enemy.lifeBar.width = maxLifeBar;
				enemy.damage = static_cast<float>(screens::screenWidth / 60);
				enemy.attackSpeed = static_cast<float>(screens::screenWidth / 60);
				enemy.sprite = sprites::mosnterEnemy;
				break;
			}
			frameRec.width = static_cast<float>(enemy.sprite.width / idleFrames);
			frameRec.height = static_cast<float>(enemy.sprite.height);
			generateElementEffectivenesses();
			enemy.attacking = false;
			currentFrame = 0;
			frameRec.x = 0;
		}
		void update(Rectangle &playerLifeBar) {
			updateEnemyrFrames();
			enemy.attackBar.width += enemy.attackSpeed*GetFrameTime();
			if (enemy.attackBar.width>=maxAttackBar) {
				attackAnimationCounter = attackMaxCounter;
				enemy.attacking = true;
				PlaySound(audio::enemyHitSfx);
				enemy.attackBar.width = 0;
				playerLifeBar.width -= enemy.damage;
			}
			if (enemy.lifeBar.width<=0) {
				enemy.alive = false;
			}
			enemyAttackAnimation();
			setEnemyDeath();
		}
		void draw(bool playerAttacking) {
			DrawRectangle(static_cast<int>(enemy.attackBar.x - screens::screenWidth / 40),
				static_cast<int>(enemy.attackBar.y - screens::screenHeight / 5),
				static_cast<int>(screens::screenWidth / 4.8f), static_cast<int>(screens::screenHeight / 1.4f), Fade(BLACK, 0.8f));
			DrawRectangle(static_cast<int>(enemy.attackBar.x), static_cast<int>(enemy.attackBar.y),
				static_cast<int>(maxAttackBar), static_cast<int>(enemy.attackBar.height), WHITE);
			DrawRectangle(static_cast<int>(enemy.lifeBar.x), static_cast<int>(enemy.lifeBar.y),
				static_cast<int>(maxLifeBar), static_cast<int>(enemy.lifeBar.height), WHITE);
			DrawRectangle(static_cast<int>(enemy.attackBar.x), static_cast<int>(enemy.attackBar.y),
				static_cast<int>(enemy.attackBar.width), static_cast<int>(enemy.attackBar.height), BLUE);
			DrawRectangle(static_cast<int>(enemy.lifeBar.x), static_cast<int>(enemy.lifeBar.y),
				static_cast<int>(enemy.lifeBar.width), static_cast<int>(enemy.lifeBar.height), RED);
			DrawRectangleLines(static_cast<int>(enemy.attackBar.x), static_cast<int>(enemy.attackBar.y),
				static_cast<int>(maxAttackBar), static_cast<int>(enemy.attackBar.height), BLACK);
			DrawRectangleLines(static_cast<int>(enemy.lifeBar.x), static_cast<int>(enemy.lifeBar.y),
				static_cast<int>(maxLifeBar), static_cast<int>(enemy.lifeBar.height), BLACK);
			DrawTexture(enemy.weakneSprite, static_cast<int>(enemy.attackBar.x),
				static_cast<int>(enemy.attackBar.y - screens::screenHeight / 5.4f), WHITE);
			DrawTexture(enemy.resistanceSprite, static_cast<int>(enemy.attackBar.x),
				static_cast<int>((enemy.attackBar.y - screens::screenHeight / 5.4f)
					+ (sprites::tileSprite1.height * 1.5f)), WHITE);
			DrawText("-> Weak", static_cast<int>(enemy.attackBar.x + sprites::tileSprite1.width),
				static_cast<int>((enemy.attackBar.y - screens::screenHeight / 5.4f) +
				(sprites::tileSprite1.height / 2 - screens::medFontSize/2)), screens::medFontSize, WHITE);
			DrawText("-> Resist", static_cast<int>(enemy.attackBar.x + sprites::tileSprite1.width),
				static_cast<int>((enemy.attackBar.y - screens::screenHeight / 5.4f) + (sprites::tileSprite1.height * 1.5f)
					+ (sprites::tileSprite1.height / 2 - screens::medFontSize / 2)), screens::medFontSize, WHITE);
			enemySpritePos.x = (enemy.attackBar.x + maxLifeBar / 2) - enemy.sprite.width/4;
			enemySpritePos.y = (enemy.attackBar.y + enemy.lifeBar.y / 2) - enemy.sprite.height / 1.19f;
			if (playerAttacking) {
				invisibleFrames();
				if (!invisible) {
					DrawTextureRec(enemy.sprite, frameRec, enemySpritePos, RED);
				}
			}
			else{
				DrawTextureRec(enemy.sprite, frameRec, enemySpritePos, WHITE);
			}
		}
		ENEMY enemy = { 0 };
	}
}