#ifndef ENEMY_H
#define ENEMY_H
#include "raylib.h"
#include "screens_manager/screens.h"
#include "sprites/sprites.h"
#include "audio/audio.h"

namespace match3 {
	namespace enemy {

		struct ENEMY {
			bool alive;
			float damage;
			int weaknessType;
			int resistanceType;
			float attackSpeed;
			Texture2D weakneSprite;
			Texture2D resistanceSprite;
			Rectangle attackBar;
			Rectangle lifeBar;
			int enemiesDefeated;
			Texture2D sprite;
			bool attacking;
		};

		void init();
		void reinitEnemy();
		void update(Rectangle &playerLifeBar);
		void draw(bool playerAttacking);
		extern ENEMY enemy;
	}
}
#endif
